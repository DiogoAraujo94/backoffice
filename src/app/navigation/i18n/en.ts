export const locale = {
    lang: 'en',
    data: {
        'NAV': {
            'PATIENTS' : 'PATIENTS',
            'APPLICATIONS': 'Applications',
            'DASHBOARDS'  : 'Dashboards',
            'CALENDAR'    : 'Calendar',
            'PROPOSALS'    : 'Proposals',
            'ECOMMERCE'   : 'E-Commerce',
            'ACADEMY'     : 'Academy',
            'MAIL'        : {
                'TITLE': 'Mail',
                'BADGE': '25'
            },
            'LIST'        : {
                'PATIENTS': 'Patients',
                'PRODUCTS': 'Products',
                'CLINICS': 'Clinics',
                'DOCTORS': 'Doctors',
                'BADGE': '25'
            },
            'MAIL_NGRX'        : {
                'TITLE': 'Mail Ngrx',
                'BADGE': '13'
            },
            'CHAT'        : 'Chat',
            'FILE_MANAGER': 'File Manager',
            'CONTACTS'    : 'Contacts',
            'TODO'        : 'To-Do',
            'SCRUMBOARD'  : 'Scrumboard'
        }
    }
};
