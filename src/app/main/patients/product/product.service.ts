import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';

@Injectable()
export class ProductService implements Resolve<any> {
    routeParams: any;
    product: any;
    onProductChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.onProductChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProduct()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get product
     *
     * @returns {Promise<any>}
     */
    getProduct(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.routeParams.id === 'new') {
                this.onProductChanged.next(false);
                resolve(false);
            } else {
                this._httpClient.get(environment.apiUrl + 'products/' + this.routeParams.id)
                    .subscribe((response: any) => {
                        this.product = response.data[0];
                        this.onProductChanged.next(this.product);
                        resolve(response);
                    }, reject);
            }
        });
    }

    /**
     * Save product
     *
     * @param product
     * @returns {Promise<any>}
     */
    saveProduct(product): Promise<any> {

        return new Promise((resolve, reject) => {
            const headers = {
                'Authorization': 'Bearer my-token',
                'Produto': product.id.toString(),
                'Codigo': product.codigo_interno,
                'Nome': product.nome,
                'Descricao': product.descricao,
                'Especialidade': product.especialidade,
                'Valor': product.valor.toString(),
                'Desconto': product.desconto.toString(),
                'Cadeira': product.cadeira.toString(),
                'Notas': product.notas
            };
            const body = {title: 'Angular POST Request Example'};
            console.log('headers', headers);
            this._httpClient.post(environment.apiUrl + 'products/update', body, {headers})
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Add product
     *
     * @param product
     * @returns {Promise<any>}
     */
    addProduct(product): Promise<any> {
        return new Promise((resolve, reject) => {
            const headers = {
                'Authorization': 'Bearer my-token',
                'Codigo': product.codigo_interno,
                'Nome': product.nome,
                'Descricao': product.descricao,
                'Especialidade': product.especialidade,
                'Valor': product.valor.toString(),
                'Desconto': product.desconto.toString(),
                'Cadeira': product.cadeira.toString(),
                'Notas': product.notas
            };
            const body = {title: 'Angular POST Request Example'};
            console.log('headers', headers);
            this._httpClient.post(environment.apiUrl + 'products', body, {headers})
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    deleteProduct(product): Promise<any> {
        return new Promise((resolve, reject) => {
            const headers = {
                'Authorization': 'Bearer my-token',
                'Id': product.id.toString()
            };
            const body = {title: 'Angular POST Request Example'};
            console.log('headers', headers);
            this._httpClient.post(environment.apiUrl + 'products/delete', body, {headers})
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
}
