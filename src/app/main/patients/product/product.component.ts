import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Location} from '@angular/common';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {FuseUtils} from '@fuse/utils';

import {ProductService} from './product.service';
import {Product} from './product.model';
import {Router} from '@angular/router';
import {UserService} from '../../login/user.service';

@Component({
    selector: 'app-products',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ProductComponent implements OnInit, OnDestroy {
    product: Product;
    pageType: string;
    productForm: FormGroup;
    especialidades: any = ['Ortodontia', 'Endodontia', 'Odontopediatria', 'Generalista', 'Implantologia', 'Prótese Fixa','Estética e Reabilitação', 'Cirurgia', 'Outras especialidades', 'Urgencia', 'Consulta Avaliação'];


    // Private
    private _unsubscribeAll: Subject<any>;
    public disabled: boolean;
    private user: any;

    /**
     * Constructor
     *
     * @param {EcommerceProductService} _ecommerceProductService
     * @param {FormBuilder} _formBuilder
     * @param {Location} _location
     * @param {MatSnackBar} _matSnackBar
     */
    constructor(
        private _ecommerceProductService: ProductService,
        private _formBuilder: FormBuilder,
        private _location: Location,
        private _matSnackBar: MatSnackBar, private _userService: UserService,
        private _router: Router
    ) {

        if (this._userService.getUser() === undefined) {
            this._router.navigate(['login'], {replaceUrl: true});
        }
        // Set the default
        this.product = new Product();
        this.user = this._userService.getUser();
        this.user.credenciais === 'USER' ? this.disabled = true : this.disabled = false;

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to update product on changes
        this._ecommerceProductService.onProductChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(product => {

                if (product) {
                    this.product = new Product(product);
                    this.pageType = 'edit';
                } else {
                    this.pageType = 'new';
                    this.product = new Product();
                }

                this.productForm = this.createProductForm();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create product form
     *
     * @returns {FormGroup}
     */
    createProductForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.product.id],
            codigo_interno: [this.product.codigo_interno],
            nome: [this.product.nome],
            descricao: [this.product.descricao],
            especialidade: [this.product.especialidade],
            valor: [this.product.valor],
            desconto: [this.product.desconto],
            cadeira: [this.product.cadeira],
            notas: [this.product.notas],
        });
    }

    /**
     * Save product
     */
    saveProduct(): void {
        const data = this.productForm.getRawValue();
        console.log(data);
        this._ecommerceProductService.saveProduct(data)
            .then(() => {

                // Trigger the subscription with new data
                this._ecommerceProductService.onProductChanged.next(data);

                // Show the success message
                this._matSnackBar.open('Produto editado com sucesso.', 'OK', {
                    verticalPosition: 'top',
                    duration: 2000
                });
                this._router.navigate(['list/products']);

            });
    }

    /**
     * Add product
     */
    addProduct(): void {
        const data = this.productForm.getRawValue();

        this._ecommerceProductService.addProduct(data)
            .then(() => {

                // Trigger the subscription with new data
                this._ecommerceProductService.onProductChanged.next(data);

                // Show the success message
                this._matSnackBar.open('Product added', 'OK', {
                    verticalPosition: 'top',
                    duration: 2000
                });
                this._router.navigate(['list/products']);

            });
    }

    deleteProduct(): void {
        const data = this.productForm.getRawValue();

        this._ecommerceProductService.deleteProduct(data)
            .then(() => {

                // Trigger the subscription with new data
                this._ecommerceProductService.onProductChanged.next(data);

                // Show the success message
                this._matSnackBar.open('Produto apagado com sucesso.', 'OK', {
                    verticalPosition: 'top',
                    duration: 2000
                });
                this._router.navigate(['list/products']);

            });

    }

    handleEspecialidade(especialidade: any): void {
        this.productForm.patchValue({especialidade: especialidade});

    }
}
