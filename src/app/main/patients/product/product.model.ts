import {MatChipInputEvent} from '@angular/material/chips';

import {FuseUtils} from '@fuse/utils';

export class Product {
    id: string;
    codigo_interno: string;
    nome: string;
    descricao: string;
    especialidade: string;
    valor: string;
    desconto: string;
    cadeira: string;
    notas: string;
    order: number;
    dente: string;

    /**
     * Constructor
     *
     * @param product
     */
    constructor(product?, dente?) {
        product = product || {};
        dente = dente || '-';
        this.id = product.id || FuseUtils.generateGUID();
        this.codigo_interno = product.codigo_interno || '';
        this.nome = product.nome || '';
        this.descricao = product.descricao || '';
        this.especialidade = product.especialidade || '';
        this.valor = product.valor || '';
        this.desconto = product.desconto || '';
        this.cadeira = product.cadeira || '';
        this.notas = product.notas || '';
        this.dente = product.dente || dente;
    }

}
