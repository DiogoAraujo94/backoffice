import {MatChipInputEvent} from '@angular/material/chips';

import {FuseUtils} from '@fuse/utils';

export class Proposal {
    id: string;
    paciente_id: string;
    pacienteNome: string;
    contacto: string;
    email: string;
    morada: string;
    postal: string;
    cidade: string;
    nacionalidade: string;
    nif: string;
    data: string;
    metodo: string;
    estado: string;
    potencial: string;
    clinica_id: string;
    cadeira: string;
    clinicaNome: string;
    valor: any;
    produtos: string;
    antes: any;
    depois: any;
    descontoTotal: any;
    total: any;
    consultas: string;
    metodos: string;
    mensalidade: string;
    taeg: string;


    /**
     * Constructor
     *
     * @param proposal
     */
    constructor(proposal?) {
        proposal = proposal || {};
        this.id = proposal.id || '';
        this.paciente_id = proposal.paciente_id || '';
        this.pacienteNome = proposal.pacienteNome || '';
        this.clinica_id = proposal.clinica_id || '';
        this.clinicaNome = proposal.clinicaNome || '';
        this.contacto = proposal.contacto || '';
        this.email = proposal.email || '';
        this.morada = proposal.morada || '';
        this.postal = proposal.postal || '';
        this.cidade = proposal.cidade || '';
        this.nacionalidade = proposal.nacionalidade || '';
        this.nif = proposal.nif || '';
        this.cadeira = proposal.cadeira || '';
        this.valor = proposal.valor ? proposal.valor : '0';
        this.metodo = proposal.metodo || '';
        this.estado = proposal.estado || '';
        this.potencial = proposal.potencial || '';
        this.produtos = proposal.produtos || '';
        this.antes = proposal.antes || '';
        this.depois = proposal.depois || '';
        this.data = proposal.data || '';
        this.consultas = proposal.consultas || '';
        this.mensalidade = proposal.mensalidade || '';
        this.taeg = proposal.taeg || '';
        this.descontoTotal = proposal.descontoTotal || null;
        this.metodos = proposal.metodos || [];
        this.total = proposal.descontoTotal ? (proposal.valor - proposal.descontoTotal) : proposal.valor;

    }

}
