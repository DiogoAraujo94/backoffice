import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {UserService} from '../../login/user.service';
import html2canvas from 'html2canvas';
import {jsPDF} from 'jspdf';
import {Consult} from '../consultas/add-consult/consult.model';


@Injectable()
export class ProposalService implements Resolve<any> {
    routeParams: any;
    patients: any[];
    patient: any;
    products: any[];
    product: any;
    doctors: any[];
    doctor: any;
    clinics: any[];
    clinic: any;
    onProductChanged: BehaviorSubject<any>;
    user: any;
    fatura: any;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     * @param _userService
     */
    constructor(
        private _httpClient: HttpClient,
        private _userService: UserService
    ) {
        // Set the defaults
        this.onProductChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProduct()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }


    getPatients(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get(environment.apiUrl + 'patients')
                .subscribe((response: any) => {
                    this.patients = response.data;
                    resolve(response.data);
                }, reject);
        });
    }

    getPatientById(id: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get(environment.apiUrl + 'patients/' + id)
                .subscribe((response: any) => {
                    this.patient = response.data[0];
                    resolve(response);
                }, reject);
        });
    }

    getProducts(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get(environment.apiUrl + 'products')
                .subscribe((response: any) => {
                    this.products = response.data;
                    resolve(response.data);
                }, reject);
        });
    }

    getProductById(id: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get(environment.apiUrl + 'products/' + id)
                .subscribe((response: any) => {
                    this.product = response.data[0];
                    resolve(response);
                }, reject);
        });
    }

    getDoctors(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get(environment.apiUrl + 'doctors')
                .subscribe((response: any) => {
                    this.doctors = response.data;
                    resolve(response.data);
                }, reject);
        });
    }

    getDoctorById(id: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get(environment.apiUrl + 'doctors/' + id)
                .subscribe((response: any) => {
                    this.doctor = response.data[0];
                    resolve(response);
                }, reject);
        });
    }

    getClinics(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get(environment.apiUrl + 'clinics')
                .subscribe((response: any) => {
                    this.clinics = response.data;
                    resolve(response.data);
                }, reject);
        });
    }

    getClinicById(id: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this._httpClient.get(environment.apiUrl + 'clinics/' + id)
                .subscribe((response: any) => {
                    this.clinic = response.data[0];
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Get product
     *
     * @returns {Promise<any>}
     */
    getProduct(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.routeParams.id === 'new') {
                this.onProductChanged.next(false);
                resolve(false);
            } else {
                this._httpClient.get(environment.apiUrl + 'proposals/' + this.routeParams.id)
                    .subscribe((response: any) => {
                        this.product = response.data[0];
                        this.onProductChanged.next(this.product);
                        resolve(response);
                    }, reject);
            }
        });
    }

    /**
     * Save product
     *
     * @param product
     * @returns {Promise<any>}
     */
    saveProduct(product): Promise<any> {

        return new Promise((resolve, reject) => {
            const headers = {
                'Authorization': 'Bearer my-token',
                'Produto': product.id.toString(),
                'Codigo': product.codigo_interno,
                'Nome': product.nome,
                'Descricao': product.descricao,
                'Especialidade': product.especialidade,
                'Valor': product.valor.toString(),
                'Desconto': product.desconto.toString(),
                'Cadeira': product.cadeira.toString(),
                'Iva': product.iva.toString(),
                'Notas': product.notas,
            };
            const body = {title: 'Angular POST Request Example'};
            console.log('headers', headers);

            this._httpClient.post(environment.apiUrl + 'products/update', body, {headers})
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Add product
     *
     * @param product
     * @returns {Promise<any>}
     */
    async addProposal(proposal, consults, metodos, invoiceElement: HTMLElement[]): Promise<any> {
        let antes = '';
        let depois = '';

        if (proposal.antes) {
            const file = proposal.antes.files[0];

            const formData = new FormData();
            formData.append('file', file);
            const res: any = await this._httpClient.post(environment.apiUrl + 'file/upload', formData, {}).toPromise();
            antes = res.file;
        }

        if (proposal.depois) {
            const file = proposal.depois.files[0];

            const formData = new FormData();
            formData.append('file', file);
            const res2: any = await this._httpClient.post(environment.apiUrl + 'file/upload', formData, {}).toPromise();
            depois = res2.file;
        }


        // upload pdf
        await this.htmlToPdf(invoiceElement, false);


        const user = this._userService.getUser();
        const headers = {
            'Authorization': 'Bearer my-token',
            patient: proposal.patient.toString(),
            pacienteNome: proposal.pacienteNome,
            contacto: proposal.contacto,
            email: proposal.email,
            morada: proposal.morada,
            postal: proposal.postal,
            cidade: proposal.cidade,
            nacionalidade: proposal.nacionalidade,
            nif: proposal.nif,
            metodo: proposal.metodo,
            estado: proposal.estado,
            potencial: proposal.potencial,
            cadeira: proposal.cadeira.toString(),
            clinica: proposal.clinica.toString(),
            clinicaNome: proposal.clinicaNome,
            total: proposal.total,
            quantities: JSON.stringify(proposal.quantities),
            usernome: user.name,
            useremail: user.email,
            userfoto: user.foto,
            usertele: user.telefone,
            antes: antes,
            depois: depois,
            descontoTotal: proposal.descontoTotal ? proposal.descontoTotal.toString() : '',
            pdf: this.fatura,
            consultas: consults,
            metodos: JSON.stringify(metodos),
            mensalidade: proposal.mensalidade ? proposal.mensalidade.toString() : '',
            taeg: proposal.taeg ? proposal.taeg.toString() : ''
        };
        const body = {title: 'Angular POST Request Example'};
        return await this._httpClient.post(environment.apiUrl + 'proposals', body, {headers}).toPromise();
    }

    async saveProposal(proposal, id, consults, metodos, invoiceElement: HTMLElement[]): Promise<any> {
        console.log('update: ', proposal);
        let antes = '';
        let depois = '';

        if (proposal.antes) {
            const file = proposal.antes.files[0];

            const formData = new FormData();
            formData.append('file', file);
            const res: any = await this._httpClient.post(environment.apiUrl + 'file/upload', formData, {}).toPromise();
            antes = res.file;
        }

        if (proposal.depois) {
            const file = proposal.depois.files[0];

            const formData = new FormData();
            formData.append('file', file);
            const res: any = await this._httpClient.post(environment.apiUrl + 'file/upload', formData, {}).toPromise();
            depois = res.file;
        }


        // upload pdf
        await this.htmlToPdf(invoiceElement, false);

        const headers = {
            'Authorization': 'Bearer my-token',
            proposta: id.toString(),
            patient: proposal.patient.toString(),
            pacienteNome: proposal.pacienteNome,
            contacto: proposal.contacto,
            email: proposal.email,
            morada: proposal.morada,
            postal: proposal.postal,
            cidade: proposal.cidade,
            nacionalidade: proposal.nacionalidade,
            nif: proposal.nif,
            metodo: proposal.metodo,
            estado: proposal.estado,
            potencial: proposal.potencial,
            cadeira: proposal.cadeira.toString(),
            clinica: proposal.clinica.toString(),
            clinicaNome: proposal.clinicaNome,
            total: proposal.total,
            antes: antes,
            depois: depois,
            descontoTotal: proposal.descontoTotal ? proposal.descontoTotal.toString() : '',
            quantities: JSON.stringify(proposal.quantities),
            consultas: consults,
            metodos: JSON.stringify(metodos),
            pdf: this.fatura,
            mensalidade: proposal.mensalidade ? proposal.mensalidade.toString() : '',
            taeg: proposal.taeg ? proposal.taeg.toString() : ''
        };
        const body2 = {title: 'Angular POST Request Example'};
        return await this._httpClient.post(environment.apiUrl + 'proposals/update', body2, {headers}).toPromise();
    }

    deleteProposal(id, proposal, patient): Promise<any> {
        const total = proposal.total.toString();
        return new Promise((resolve, reject) => {
            const headers = {
                'Authorization': 'Bearer my-token',
                'Id': id.toString(),
                'Valor': total.slice(0, -1),
                'Patient': patient.toString()
            };
            const body = {title: 'Angular POST Request Example'};
            this._httpClient.post(environment.apiUrl + 'proposals/delete', body, {headers})
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    async duplicateProposal(proposal: any): Promise<any> {
        const user = this._userService.getUser();
        return new Promise((resolve, reject) => {
            const headers = {
                'Authorization': 'Bearer my-token',
                patient: proposal.patient.toString(),
                pacienteNome: proposal.pacienteNome,
                contacto: proposal.contacto,
                email: proposal.email,
                morada: proposal.morada,
                postal: proposal.postal,
                cidade: proposal.cidade,
                nacionalidade: proposal.nacionalidade,
                nif: proposal.nif,
                metodo: proposal.metodo,
                estado: proposal.estado,
                potencial: proposal.potencial,
                cadeira: proposal.cadeira.toString(),
                clinica: proposal.clinica.toString(),
                clinicaNome: proposal.clinicaNome,
                total: proposal.total,
                quantities: JSON.stringify(proposal.quantities),
                usernome: user.name,
                useremail: user.email,
                userfoto: user.foto,
                usertele: user.telefone,
                antes: '',
                depois: '',
                descontoTotal: proposal.descontoTotal ? proposal.descontoTotal.toString() : '',
                pdf: '',
                metodos: '[]',
                mensalidade: proposal.mensalidade.toString(),
                taeg: proposal.taeg.toString(),
            };
            const body = {title: 'Angular POST Request Example'};
            console.log('headers', headers);
            this._httpClient.post(environment.apiUrl + 'proposals', body, {headers})
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }


    async htmlToPdf(elements: HTMLElement[], openPDF = true): Promise<void> {
        const pdf = new jsPDF({ unit: 'px' });

        try {
            for (const element of elements) {

                const pageId = 'invoice-page-' + pdf.getCurrentPageInfo().pageNumber;
                const clone = element.cloneNode(true) as HTMLElement;
                clone.style.position = 'absolute';
                clone.style.top = '-999999px';
                clone.style.width = '1020px';
                clone.style.height = '1443px';
                clone.id = pageId;

                document.getElementsByTagName('BODY')[0].append(clone);

                const clonedElement = document.querySelector('#' + pageId) as HTMLElement;
                const canvas = await html2canvas(clonedElement, { width: 1020, height: 1443, windowWidth: 1920, windowHeight: 1080});

                clonedElement.remove();

                const imgData = canvas.toDataURL('image/jpeg', 1.0);
                pdf.addImage(imgData, 0, 0, pdf.internal.pageSize.width, pdf.internal.pageSize.height);

                if (element !== elements[elements.length - 1]) {
                    pdf.addPage();
                }
            }
        } catch (e) {
            console.error(e);
        }

        const blob = pdf.output('blob');

        const formData = new FormData();
        formData.append('file', blob);

        const res = await this._httpClient.post(environment.apiUrl + 'file/upload', formData, {}).toPromise() as any;
        this.fatura = res.file;

        if (openPDF) {
            window.open(this.fatura, '_blank');
        }
    }

    saveState(id: any, data: any): Promise<any> {
        return new Promise((resolve, reject) => {
            const headers = {
                'Authorization': 'Bearer my-token',
                'proposta': id.toString(),
                metodo: data.metodo,
                estado: data.estado
            };
            const body = {title: 'Angular POST Request Example'};
            console.log('headers', headers);
            this._httpClient.post(environment.apiUrl + 'proposals/state', body, {headers})
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });

    }

    addConsult(consult: Consult): void {


        // no futuro retornar promessa e por aqui a chamada ao backend
        console.log(consult);
    }


}