import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {Location} from '@angular/common';
import {MatSnackBar} from '@angular/material/snack-bar';
import {BehaviorSubject, merge, Observable, Subject} from 'rxjs';
import {map, takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {FuseUtils} from '@fuse/utils';

import {ProposalService} from './proposal.service';
import {Proposal} from './proposal.model';
import {Router} from '@angular/router';
import {Patient} from '../patient/patient.model';
import {Product} from '../product/product.model';
import {Clinic} from '../clinic/clinic.model';
import {DataSource} from '@angular/cdk/collections';
import {ProposalsService} from '../proposals/proposals.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {UserService} from '../../login/user.service';
import {MatTab} from '@angular/material/tabs';
import {PatientComponent} from '../patient/patient.component';
import {MatDialog} from '@angular/material/dialog';
import {PatientService} from '../patient/patient.service';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {FusePerfectScrollbarDirective} from '../../../../@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {Consult} from '../consultas/add-consult/consult.model';

type Treatment = {
         id: string;
         name: string;
};

// Is there any special reason for this to be here and not inside the component?
const prodsSelected: any[] = [];


@Component({
    selector: 'e-commerce-products',
    templateUrl: './proposal.component.html',
    styleUrls: ['./proposal.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ProposalComponent implements OnInit, OnDestroy {
    dataSource: FilesDataSource2 | null;
    patients: any[] = [];
    patient: Patient = new Patient();
    doctors: any[];
    clinics: any[];
    clinic: Clinic = new Clinic();
    products: Product[];
    metodo: string;
    metodos = ['12x s/juros', '24x s/juros', '36x s/juros', '48x + TAEG'];
    dentesAdulto1 = ['18', '17', '16', '15', '14', '13', '12', '11'];
    dentesAdulto2 = ['48', '47', '46', '45', '44', '43', '42', '41'];
    dentesAdulto3 = ['21', '22', '23', '24', '25', '26', '27', '28'];
    dentesAdulto4 = ['31', '32', '33', '34', '35', '36', '37', '38'];
    dentesCrianca1 = ['55', '54', '53', '52', '51'];
    dentesCrianca2 = ['85', '84', '83', '82', '81'];
    dentesCrianca3 = ['61', '62', '63', '64', '65'];
    dentesCrianca4 = ['71', '72', '73', '74', '75'];

    dentesChecked = new Map<string, boolean>();

    potencial: string;
    productsSelected: any[] = [];
    product: Product = new Product();
    productForm: FormGroup;
    total = 0;
    disabled = false;
    disabledUser: boolean;
    private user: any;
    @ViewChild('tabFactura')
    private tabFactura: MatTab;
    private dialogRef: any;
    @ViewChild(FusePerfectScrollbarDirective)
    listScroll: FusePerfectScrollbarDirective;
    consultas: { [key: string]: Treatment[] } = {};
    consults: Consult[] = [];
    public currentDiscont: any;
    public initialDiscount: any;
    public enableMensalidade = false;
    invoiceElementSelector = '.invoice-container .card';
    generatingPdf = false;
    public clinicNome: string;
    availableTreatments: Treatment[];


    /**
     * Constructor
     *
     * @param {ProposalService} _proposalService
     * @param {FormBuilder} _formBuilder
     * @param {Location} _location
     * @param {MatSnackBar} _matSnackBar
     */
    constructor(
        private patientService: PatientService,
        private _matDialog: MatDialog,
        private _proposalService: ProposalService,
        private _formBuilder: FormBuilder,
        private _location: Location,
        private _matSnackBar: MatSnackBar,
        private _userService: UserService,
        private _router: Router
    ) {

        if (this._userService.getUser() === undefined) {
            this._router.navigate(['login'], {replaceUrl: true});
        }
        this.user = this._userService.getUser();
        this.user.credenciais === 'USER' ? this.disabledUser = true : this.disabledUser = false;

        this.productForm = this._formBuilder.group({
            id: '',
            patient: '',
            pacienteNome: '',
            contacto: '',
            email: '',
            morada: '',
            postal: '',
            desconto: '',
            cidade: '',
            nacionalidade: '',
            nif: '',
            metodo: '',
            clinica: '',
            cadeira: '',
            clinicaNome: '',
            estado: '',
            potencial: '',
            total: 0,
            quantities: this._formBuilder.array([]),
            antes: [],
            depois: [],
            descontoTotal: '',
            mensalidade: '',
            taeg: ''
        });
        // Set the default
        this.proposal = new Proposal();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    proposal: Proposal;
    pageType: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    private active: boolean;
    private fakePatient: Patient;
    filteredProducts: Product[] = [];
    filteredProductsTimeOut: any;
    filteredPatientsTimeOut: any;
    filteredPatients: any[] = [];
    checked = true;
    selected: number;
    metodosChecked = [];
    
    /**
     * On init
     */
    ngOnInit(): void {
        this.dentesAdulto1.forEach(dente => this.dentesChecked.set(dente, false));
        this.dentesAdulto2.forEach(dente => this.dentesChecked.set(dente, false));
        this.dentesAdulto3.forEach(dente => this.dentesChecked.set(dente, false));
        this.dentesAdulto4.forEach(dente => this.dentesChecked.set(dente, false));
        this.dentesCrianca1.forEach(dente => this.dentesChecked.set(dente, false));
        this.dentesCrianca2.forEach(dente => this.dentesChecked.set(dente, false));
        this.dentesCrianca3.forEach(dente => this.dentesChecked.set(dente, false));
        this.dentesCrianca4.forEach(dente => this.dentesChecked.set(dente, false));

        this._proposalService.getPatients().then(data => {
            console.log(data);
            this.patients = data;
            this.filteredPatients = data;
            this.fakePatient = new Patient();
            this.fakePatient.nome = '-';
            this.patients.push(this.fakePatient);
        });
        this._proposalService.getClinics().then(data => {
            console.log(data);
            this.clinics = data;
        });
        this._proposalService.getDoctors().then(data => {
            console.log(data);
            this.doctors = data;
        });
        this._proposalService.getProducts().then(data => {
            this.products = data;
            this.filteredProducts = data;
        });

        // Subscribe to update product on changes
        this._proposalService.onProductChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(proposal => {

                if (proposal) {
                    this.proposal = new Proposal(proposal);
                    this.productForm = this.createProposalForm();
                    this.metodo = this.proposal.metodo;
                    this.clinicNome = this.proposal.clinicaNome;

                    this.metodosChecked = JSON.parse(proposal.metodos) ? JSON.parse(proposal.metodos) : [];
                    this.metodosChecked.forEach(met => {
                        if (met.includes('48x')) {
                            this.enableMensalidade = true;
                        }
                    });
                    console.log('proposal: ', this.proposal);
                    const prods = JSON.parse(this.proposal.produtos);
                    this.currentDiscont = proposal.descontoTotal;
                    this.initialDiscount = proposal.descontoTotal;
                    console.log('prods: ', prods);
                    prods.forEach(prod => this.addQuantity(new Product(prod)));
                    this.updateAvailableTreatments();


                    this.pageType = 'edit';
                    this.consults = proposal.consultas ? JSON.parse(proposal.consultas) : [];
                } else {
                    this.pageType = 'new';
                    this.proposal = new Proposal();
                    this.productForm = this.createProposalForm();

                }
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    /**
     * Create product form
     *
     * @returns {FormGroup}
     */
    createProposalForm(): FormGroup {
        return this._formBuilder.group({
            id: '',
            patient: [this.proposal.paciente_id],
            pacienteNome: [this.proposal.pacienteNome],
            contacto: [this.proposal.contacto],
            email: [this.proposal.email],
            morada: [this.proposal.morada],
            postal: [this.proposal.postal],
            desconto: '',
            quantidade: 1,
            cidade: [this.proposal.cidade],
            nacionalidade: [this.proposal.nacionalidade],
            nif: [this.proposal.nif],
            metodo: [this.proposal.metodo],
            clinica: [this.proposal.clinica_id],
            cadeira: [this.proposal.cadeira],
            clinicaNome: [this.proposal.clinicaNome],
            estado: [this.proposal.estado],
            potencial: [this.proposal.potencial],
            total: [this.proposal.valor],
            antes: [],
            depois: [],
            descontoTotal: [this.proposal.descontoTotal],
            mensalidade: [this.proposal.mensalidade],
            taeg: [this.proposal.taeg],
            quantities: this._formBuilder.array([]),

        });
    }

    validateForm(): boolean {
        const data = this.productForm.getRawValue();
        if (data.descontoTotal !== '' || this.proposal.total !== data.total - data.descontoTotal) {
            this.proposal.descontoTotal = data.descontoTotal;
            this.proposal.valor = data.total;
            this.proposal.total = data.total - data.descontoTotal;
        }
        this.active = true;
        // usar em caso de se querer forçar a que a aba da fatura esteja aberta
        // this.active = this.tabFactura ? this.tabFactura.isActive : false;
        return !(this.productForm.valid && this.active);
    }

    handlePatient(patient: Patient): void {
        this.patient = patient;
        this.disabled = this.validateForm();
        this.productForm.patchValue({patient: patient.id.toString()});
        this.productForm.patchValue({pacienteNome: this.patient.nome});
        this.productForm.patchValue({contacto: this.patient.contacto});
        this.productForm.patchValue({email: this.patient.email});
        this.productForm.patchValue({morada: this.patient.morada});
        this.productForm.patchValue({postal: this.patient.postal});
        this.productForm.patchValue({cidade: this.patient.cidade});
        this.productForm.patchValue({nacionalidade: this.patient.pais});
        this.productForm.patchValue({nif: this.patient.nif});
        this.proposal.pacienteNome = this.patient.nome;
        this.proposal.morada = this.patient.morada;
        this.proposal.contacto = this.patient.contacto;
    }

    handleProduto(id: string): void {
        this._proposalService.getProductById(id).then(data => {
            this.product = data.data[0];
            this.product.id = id;
        });

        this.disabled = this.validateForm();

    }

    quantities(): FormArray {
        return this.productForm.get('quantities') as FormArray;
    }

    newQuantity(prod: Product, dente: string): FormGroup {
        prod.dente = dente;
        prod.order = prodsSelected.length + 1;
        this.productsSelected.push(prod);
        prodsSelected.push(prod);

        return this._formBuilder.group({
            id: prod.id.toString(),
            nome: prod.nome.toString(),
            dente: prod.dente,
            valor: prod.desconto ? prod.desconto : prod.valor,
            especialidade: prod.especialidade.toString()
        });
    }

    addQuantity(prod: Product): void {
        const prod2 = new Product(prod);
        const desconto = this.productForm.getRawValue().desconto;
        desconto !== '' ? prod2.valor = desconto : prod2.desconto = undefined;
        this.quantities().push(this.newQuantity(prod2, prod2.dente ? prod2.dente : '-'));
        this.total += Number(prod2.valor);
        this.productForm.patchValue({total: this.total.toString()});
        this.disabled = this.validateForm();


    }

    addQuantityButton(prod: Product): void {
        let prod2 = new Product(prod);
        let contador = 0;
        let quantidade = this.productForm.getRawValue().quantidade;

        if (quantidade > 1) {
            while (quantidade > 0) {
                const desconto = this.productForm.getRawValue().desconto;
                desconto !== '' ? prod2.valor = desconto : prod2.desconto = undefined;
                this.addQuantity(prod2);
                this.total += Number(prod2.valor);
                this.productForm.patchValue({total: this.total.toString()});
                this.disabled = this.validateForm();
                quantidade --;
            }
        }

        this.dentesChecked.forEach((valor, dente) => {
            if (valor) {
                prod2 = new Product(prod, dente);
                const desconto = this.productForm.getRawValue().desconto;
                desconto !== '' ? prod2.valor = desconto : prod2.desconto = undefined;
                contador++;
                this.dentesChecked.set(dente, false);
                this.quantities().push(this.newQuantity(prod2, dente));
                this.total += Number(prod2.valor);
                this.productForm.patchValue({total: this.total.toString()});
                this.disabled = this.validateForm();
            }
        });
        if (contador === 0) {
            this.addQuantity(prod2);
        }
        // LIMPAR CAMPO DO DESCONTO
        this.productForm.patchValue({desconto: ''});
        this.productForm.patchValue({quantidade: 1});

        this.updateAvailableTreatments();

        // TOAST FINAL
        this._matSnackBar.open('Produto adicionado com sucesso.', 'OK', {
            verticalPosition: 'top',
            duration: 2000
        });
    }


    removeQuantity(i: number): void {
        const prod = this.productsSelected[i];
        this.total -= prod.desconto ? Number(prod.desconto) : Number(prod.valor);
        this.productForm.patchValue({total: this.total.toString()});
        this.quantities().removeAt(i);
        this.productsSelected.splice(i, 1);
        prodsSelected.splice(i, 1);
        this.disabled = this.validateForm();
    }

    deleteProposal(): void {
        const data = this.productForm.getRawValue();
        this._proposalService.deleteProposal(this.proposal.id, data, this.proposal.paciente_id)
            .then(() => {

                // Trigger the subscription with new data
                this._proposalService.onProductChanged.next(data);

                // Show the success message
                this._matSnackBar.open('Proposta apagada com sucesso.', 'OK', {
                    verticalPosition: 'top',
                    duration: 2000
                });
                this._router.navigate(['list/proposals']);

            });

    }

    /**
     * Add proposal
     */

    addProposal(): void {
        const data = this.productForm.getRawValue();
        const consults = JSON.stringify(this.consults);

        this._proposalService.addProposal(data, consults, this.metodosChecked, this.getInvoicePagesElements())
            .then(() => {

                // Trigger the subscription with new data
                this._proposalService.onProductChanged.next(data);

                // Show the success message
                this._matSnackBar.open('Proposta adicionada com sucesso', 'OK', {
                    verticalPosition: 'top',
                    duration: 2000
                });
                this._router.navigate(['list/proposals']);

            });
    }


    duplicateProposal(): void {
        const data = this.productForm.getRawValue();

        this._proposalService.duplicateProposal(data)
            .then(() => {

                // Trigger the subscription with new data
                this._proposalService.onProductChanged.next(data);

                // Show the success message
                this._matSnackBar.open('Proposta editada com sucesso', 'OK', {
                    verticalPosition: 'top',
                    duration: 2000
                });
                this._router.navigate(['list/proposals']);

            });

    }

    saveProposal(): void {
        const data = this.productForm.getRawValue();
        this.proposal.total = data.total;
        this.proposal.valor = data.valor;
        this.proposal.descontoTotal = data.descontoTotal;
        const consults = JSON.stringify(this.consults);

        this._proposalService
            .saveProposal(data, this.proposal.id, consults, this.metodosChecked, this.getInvoicePagesElements())
            .then(() => {

                // Trigger the subscription with new data
                this._proposalService.onProductChanged.next(data);

                // Show the success message
                this._matSnackBar.open('Proposta editada com sucesso', 'OK', {
                    verticalPosition: 'top',
                    duration: 2000
                });
                this._router.navigate(['list/proposals']);

            });
    }


    handleClinica(id: string): void {

        this._proposalService.getClinicById(id).then(data => {
            this.clinic = data.data[0];
            this.clinicNome = this.clinic.nome;
            this.productForm.patchValue({clinica: id.toString()});
            this.productForm.patchValue({clinicaNome: this.clinic.nome});
        });
        this.disabled = this.validateForm();

    }


    handleMetodo(metodo: any): void {
        let count = 0;
        if (!this.metodosChecked.includes(metodo)) {
            this.metodosChecked.push(metodo);
        } else {
            this.metodosChecked.splice(this.metodosChecked.indexOf(metodo), 1);
        }
        this.metodosChecked.forEach(met => {
            if (met.includes('48x')) {
                count ++;
                this.enableMensalidade = true;
            }
        });
        if (count === 0) {
            this.enableMensalidade = false;
        }

    }

    htmlToPdf(): void {
        this.generatingPdf = true;
        this._proposalService.htmlToPdf(this.getInvoicePagesElements()).then(() => this.generatingPdf = false);
    }

    openCreateDialog(): void {
        this.patientService.onProductChanged.next(false);
        this.dialogRef = this._matDialog.open(PatientComponent, {
            width: '80%',
            autoFocus: false,
            maxHeight: '95vh',
            panelClass: 'knowledgebase-article-dialog',
            data: {}
        });

    }

    onDrop(event: CdkDragDrop<string[]>): void {
        moveItemInArray(this.productsSelected, event.previousIndex, event.currentIndex);
        this.productsSelected.forEach((user, idx) => {
            user.order = idx + 1;
        });
    }

    filterProductOptions(event: Event): void {
        if (this.filteredProductsTimeOut) {
            clearTimeout(this.filteredProductsTimeOut);
        }

        this.filteredProductsTimeOut = setTimeout(() => {

            const input = (event.target as HTMLInputElement).value.toLowerCase();
            console.log('Barunca: ', input);
            const inputSplited = input.split(' ');
            this.filteredProducts = this.products;
            inputSplited.forEach(term => {
            this.filteredProducts = this.filteredProducts
                .filter(prod =>
                    prod.nome.toLowerCase()
                        .normalize('NFD')
                        .replace(/[\u0300-\u036f]/g, '')
                        .includes(term.normalize('NFD')
                            .replace(/[\u0300-\u036f]/g, '')), 500);

            });
        });
    }

    filterPatientOptions(event: Event): void {
        if (this.filteredPatientsTimeOut) {
            clearTimeout(this.filteredPatientsTimeOut);
        }

        this.filteredPatientsTimeOut = setTimeout(() =>
            this.filteredPatients = this.patients.filter(pac => pac.nome.toLowerCase().includes((event.target as HTMLInputElement).value.toLocaleLowerCase())), 500);

    }

    /**
     * On list add
     *
     * @param newListName
     */
    onListAdd(newListName): void {
        if (newListName === '') {
            return;
        }
        const consulta = new Consult({name: newListName});
        this.consultas[consulta.id] = [];
        this.consults.push(consulta);
        this._proposalService.addConsult(consulta);
        this.updateAvailableTreatments();
    }

    handleDente(dente: any): void {
        const value = this.dentesChecked.get(dente);
        this.dentesChecked.set(dente, !value);
    }

    /**
     * Get selected products grouped by 'pages' of 15 max products
     */
    getSelectedProductsPages(): any[][] {
        const pages = [];

        let page = [];
        this.productsSelected.forEach((product, index) => {
            page.push(product);
            const isLastProduct = (index + 1) === this.productsSelected.length;
            if (isLastProduct || (index + 1) % 15 === 0) {
                if (isLastProduct && page.length > 10) {
                    pages.push(page.slice(0, 10));
                    pages.push(page.slice(10, page.length));
                } else {
                    pages.push(page);
                }

                page = [];
            }
        });

        return pages;
    }

    getInvoicePagesElements(): HTMLElement[] {
        const elements = [];

        const queryElements = document.querySelectorAll(this.invoiceElementSelector);
        queryElements.forEach(value => elements.push(value));

        return elements;
    }

    getTotalPages(): number {
        return Math.ceil(this.productsSelected.length / 15) + Math.floor((this.productsSelected.length % 15) / 11);
    }

    getClinicaMorada(name: string): string {
        switch (name) {
            case 'Clinica Minho': {
                return 'Av. Alcaide de Faria, nº297';
            }
            case 'Clínica Douro': {
                return 'R. Leira da Relva, nº117';
            }
            case 'Clínica Tejo': {
                return 'Av. João Crisóstomo, nº13';
            }
        }


    }

    getClinicaContacto(name: string): string {
        switch (name) {
            case 'Clinica Minho': {
                return '927 202 141';
            }
            case 'Clínica Douro': {
                return '926 001 615';
            }
            case 'Clínica Tejo': {
                return '960 412 937';
            }
        }
    }

    getClinicPostalCode(name: string): string {
        switch (name) {
            case 'Clinica Minho': {
                return '4750-106 Barcelos';
            }
            case 'Clínica Douro': {
                return '4410-155 Vila Nova de Gaia';
            }
            case 'Clínica Tejo': {
                return '1000-093 Lisboa';
            }
        }
    }

    handleConsultTreatmentChange(id: string, treatments: any[]): void {
        this.consultas[id] = treatments;
        this.updateAvailableTreatments();
    }

    private updateAvailableTreatments(): void {
        this.availableTreatments = this.productsSelected.filter(prod =>
            !this.consults.some(consult =>
                this.consultas[consult.id].some(treatment =>
                    treatment.name.split('|')[0].trim() === prod.nome
                )
            )
        );
    }

}

export class FilesDataSource2 extends DataSource<any> {
    private _filterChange = new BehaviorSubject('');
    private _filteredDataChange = new BehaviorSubject('');

    /**
     * Constructor
     *
     * @param {_proposalsService} _proposalsService
     * @param {MatPaginator} _matPaginator
     * @param {MatSort} _matSort
     */
    constructor(
        private _proposalsService: ProposalsService,
        private _matPaginator: MatPaginator,
        private _matSort: MatSort
    ) {
        super();

        this.filteredData = prodsSelected;
    }

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            this._proposalsService.onProductsChanged,
            this._matPaginator.page,
            this._filterChange,
            this._matSort.sortChange
        ];

        return merge(...displayDataChanges)
            .pipe(
                map(() => {
                        const data = prodsSelected.slice();

                        this.filteredData = [...data];

                        // Grab the page's slice of data.
                        const startIndex = this._matPaginator.pageIndex * this._matPaginator.pageSize;
                        return data.splice(startIndex, this._matPaginator.pageSize);
                    }
                ));
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    // Filtered data
    get filteredData(): any {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any) {
        this._filteredDataChange.next(value);
    }

    // Filter
    get filter(): string {
        return this._filterChange.value;
    }

    set filter(filter: string) {
        this._filterChange.next(filter);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Filter data
     *
     * @param data
     * @returns {any}
     */
    filterData(data): any {
        if (!this.filter) {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    /**
     * Sort data
     *
     * @param data
     * @returns {any[]}
     */
    sortData(data): any[] {
        if (!this._matSort.active || this._matSort.direction === '') {
            return data;
        }

        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';

            switch (this._matSort.active) {
                case 'id':
                    [propertyA, propertyB] = [a.id, b.id];
                    break;
                case 'name':
                    [propertyA, propertyB] = [a.name, b.name];
                    break;
                case 'categories':
                    [propertyA, propertyB] = [a.categories[0], b.categories[0]];
                    break;
                case 'price':
                    [propertyA, propertyB] = [a.priceTaxIncl, b.priceTaxIncl];
                    break;
                case 'quantity':
                    [propertyA, propertyB] = [a.quantity, b.quantity];
                    break;
                case 'active':
                    [propertyA, propertyB] = [a.active, b.active];
                    break;
            }

            const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._matSort.direction === 'asc' ? 1 : -1);
        });
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}

