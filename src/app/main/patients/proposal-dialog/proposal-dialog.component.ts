import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Proposal} from '../proposal/proposal.model';
import {ProposalService} from '../proposal/proposal.service';

@Component({
    selector: 'fuse-confirm-dialog',
    templateUrl: './proposal-dialog.component.html',
    styleUrls: ['./proposal-dialog.component.scss']
})
export class ProposalDialogComponent implements OnInit {
    public proposal: Proposal;
    proposalDialogFom: FormGroup;
    metodo: string;
    estados: any[] = ['Pendente', 'Aceite', 'Rejeitada'];
    metodos: any[] = ['Pronto Pagamento', '12x s/juros', '24x s/juros', '36x s/juros', '48x + TAEG'];

    ngOnInit(): void {

        this.proposalDialogFom = this.createProposalDialogForm();
        this.metodo = this.proposal.metodo;
    }


    /**
     * Constructor
     *
     * @param proposalService
     * @param _formBuilder
     * @param matDialogRef
     * @param _data
     */
    constructor(
        private proposalService: ProposalService,
        private _formBuilder: FormBuilder,
        public matDialogRef: MatDialogRef<ProposalDialogComponent>,
        @Inject(MAT_DIALOG_DATA) private _data: any,
    ) {
        this.proposal = _data.proposal;
        if (this.proposal.metodos !== '') {
            this.metodos = JSON.parse(this.proposal.metodos);
        }
        console.log('dialog: ', this.proposal);
    }

    createProposalDialogForm(): FormGroup {
        return this._formBuilder.group({
            estado: [this.proposal.estado],
            metodo: [this.proposal.metodo],
        });
    }

    handleEstado(estado: any): void {

        this.proposalDialogFom.patchValue({estado: estado});

    }

    handleMetodo(metodo: any): void {

        this.proposalDialogFom.patchValue({metodo: metodo});

    }

    closeDialog(): void {
        this.matDialogRef.close();

    }

    saveState(id: any): void {
        const data = this.proposalDialogFom.getRawValue();
        this.proposalService.saveState(id, data).then(() => {
                this.matDialogRef.close();
                window.location.reload();
            }
        );

    }
}
