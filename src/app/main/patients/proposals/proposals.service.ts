import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {UserService} from '../../login/user.service';

@Injectable()
export class ProposalsService implements Resolve<any> {
    proposals: any[];
    onProductsChanged: BehaviorSubject<any>;
    private user: any;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _userService: UserService,
    ) {

        // Set the defaults
        this.onProductsChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProposals()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get products
     *
     * @returns {Promise<any>}
     */
    getProposals(): Promise<any> {
        this.user = this._userService.getUser();
        const headers = {
            'Authorization': 'Bearer my-token',
            'User': this.user.credenciais,
            email: this.user.email,
            clinica: this.user.clinica ? this.user.clinica : '',
        };
        return new Promise((resolve, reject) => {
            this._httpClient.get(environment.apiUrl + 'proposals', {headers})
                .subscribe((response: any) => {
                    this.proposals = response.data;
                    this.proposals.reverse();
                    this.onProductsChanged.next(this.proposals);
                    resolve(response.data);
                }, reject);
        });
    }
}
