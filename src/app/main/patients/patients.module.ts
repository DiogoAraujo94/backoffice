import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListComponent} from './list/list.component';
import {RouterModule} from '@angular/router';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatIconModule} from '@angular/material/icon';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {FuseSharedModule} from '../../../@fuse/shared.module';
import {MatDialogModule} from '@angular/material/dialog';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatMenuModule} from '@angular/material/menu';
import {MatRippleModule} from '@angular/material/core';
import {MatSelectModule} from '@angular/material/select';
import {ProductsComponent} from './products/products.component';
import {FuseMaterialColorPickerModule, FuseSearchBarModule, FuseWidgetModule} from '../../../@fuse/components';
import {MatTableModule} from '@angular/material/table';
import {ProductsService} from './products/products.service';
import {MatChipsModule} from '@angular/material/chips';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTabsModule} from '@angular/material/tabs';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {AgmCoreModule} from '@agm/core';
import {ProductComponent} from './product/product.component';
import {ProductService} from './product/product.service';
import {DoctorComponent} from './doctor/doctor.component';
import {DoctorService} from './doctor/doctor.service';
import {ListService} from './list/list.service';
import {DoctorsComponent} from './doctors/doctors.component';
import {DoctorsService} from './doctors/doctors.service';
import {PatientComponent} from './patient/patient.component';
import {PatientService} from './patient/patient.service';
import {ClinicsComponent} from './clinics/clinics.component';
import {ClinicsService} from './clinics/clinics.service';
import {ClinicService} from './clinic/clinic.service';
import {ClinicComponent} from './clinic/clinic.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {ProposalsComponent} from './proposals/proposals.component';
import {ProposalsService} from './proposals/proposals.service';
import {ProposalService} from './proposal/proposal.service';
import {ProposalComponent} from './proposal/proposal.component';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatDividerModule} from '@angular/material/divider';
import {DatePipe} from '@angular/common';
import {MaterialFileInputModule} from 'ngx-material-file-input';
import {MatStepperModule} from '@angular/material/stepper';
import {MatRadioModule} from '@angular/material/radio';
import {ProposalDialogComponent} from './proposal-dialog/proposal-dialog.component';
import {NgxDnDModule} from '@swimlane/ngx-dnd';
import {MatTooltipModule} from '@angular/material/tooltip';
import {EditConsultNameComponent} from './consultas/edit-consult-name/edit-consult-name.component';
import {ConsultasComponent} from './consultas/consultas.component';
import {AddConsultComponent} from './consultas/add-consult/add-consult.component';
import {ConsultComponent} from './consultas/consult/consult.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {AddConsultListComponent} from './consultas/add-consult-list/add-consult-list.component';
import {ConsultasService} from './consultas/consultas.service';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';


const routes = [
    {
        path     : 'patients',
        component: ListComponent,
        resolve  : {
            data: ListService
        }
    },
    {
        path     : 'patients/:id',
        component: PatientComponent,
        resolve  : {
            data: PatientService
        }
    },
    {
        path     : 'patients/:id/:handle',
        component: PatientComponent,
        resolve  : {
            data: PatientService
        }
    },
    {
        path     : 'products',
        component: ProductsComponent,
        resolve  : {
            data: ProductsService
        }
    },
    {
        path     : 'products/:id',
        component: ProductComponent,
        resolve  : {
            data: ProductService
        }
    },
    {
        path     : 'products/:id/:handle',
        component: ProductComponent,
        resolve  : {
            data: ProductService
        }
    },
    {
        path     : 'doctors',
        component: DoctorsComponent,
        resolve  : {
            data: DoctorsService
        }
    },
    {
        path     : 'doctors/:id',
        component: DoctorComponent,
        resolve  : {
            data: DoctorService
        }
    },
    {
        path     : 'doctors/:id/:handle',
        component: DoctorComponent,
        resolve  : {
            data: DoctorService
        }
    },
    {
        path     : 'clinics',
        component: ClinicsComponent,
        resolve  : {
            data: ClinicsService
        }
    },
    {
        path     : 'clinics/:id',
        component: ClinicComponent,
        resolve  : {
            data: ClinicService
        }
    },
    {
        path     : 'clinics/:id/:handle',
        component: ClinicComponent,
        resolve  : {
            data: ClinicService
        }
    },
    {
        path     : 'proposals',
        component: ProposalsComponent,
        resolve  : {
            data: ProposalsService
        }
    },
    {
        path     : 'proposals/:id',
        component: ProposalComponent,
        resolve  : {
            data: ProposalService
        }
    },
    {
        path     : 'proposals/:id/:handle',
        component: ProposalComponent,
        resolve  : {
            data: ProposalService
        }
    },
];


@NgModule({
  declarations: [
      ListComponent,
      ProductsComponent,
      PatientComponent,
      ProductComponent,
      DoctorsComponent,
      DoctorComponent,
      ClinicsComponent,
      ClinicComponent,
      ProposalsComponent,
      ProposalComponent,
      ProposalDialogComponent,
      AddConsultComponent,
      ConsultComponent,
      EditConsultNameComponent,
      ConsultasComponent,
      AddConsultListComponent
  ],
    imports: [
        RouterModule.forChild(routes),
        CommonModule,

        MatButtonModule,
        MatCheckboxModule,
        MatDialogModule,
        MatFormFieldModule,
        MatIconModule,

        MatChipsModule,
        MatExpansionModule,
        MatPaginatorModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSelectModule,
        MatToolbarModule,
        MaterialFileInputModule,


        NgxDatatableModule,

        FuseSharedModule,
        FuseSearchBarModule,
        MatTableModule,

        RouterModule.forChild(routes),

        NgxChartsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
        }),

        FuseSharedModule,
        FuseWidgetModule,
        MatDatepickerModule,
        MatAutocompleteModule,
        MatDividerModule,
        MatStepperModule,
        MatRadioModule,
        FuseMaterialColorPickerModule,
        DragDropModule,
        NgxDnDModule,
        MatTooltipModule,
        MatSlideToggleModule,


    ],

    providers   : [
        ProductsService,
        ProductService,
        ListService,
        DoctorService,
        DoctorsService,
        PatientService,
        ClinicsService,
        ClinicService,
        ProposalsService,
        ProposalService,
        ConsultasService,
        DatePipe
    ],

    entryComponents: [
        ProposalDialogComponent
    ],

})
export class PatientsModule { }
