import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Location} from '@angular/common';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';

import {ClinicService} from './clinic.service';
import {Clinic} from './clinic.model';
import {Router} from '@angular/router';
import {UserService} from '../../login/user.service';

@Component({
    selector: 'app-products',
    templateUrl: './clinic.component.html',
    styleUrls: ['./clinic.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ClinicComponent implements OnInit, OnDestroy {
    clinic: Clinic;
    pageType: string;
    doctorForm: FormGroup;

    // Private
    private _unsubscribeAll: Subject<any>;
    disabled: boolean;
    private user: any;

    /**
     * Constructor
     *
     * @param _clinicService
     * @param {FormBuilder} _formBuilder
     * @param {Location} _location
     * @param _router
     * @param {MatSnackBar} _matSnackBar
     * @param _userService
     */
    constructor(
        private _clinicService: ClinicService,
        private _formBuilder: FormBuilder,
        private _location: Location,
        private _matSnackBar: MatSnackBar,
        private _userService: UserService,
        private _router: Router
    ) {

        if (this._userService.getUser() === undefined) {
            this._router.navigate(['login'], {replaceUrl: true});
        }
        this.user = this._userService.getUser();
        this.user.credenciais === 'USER' ? this.disabled = true : this.disabled = false;
        if (this.user.credenciais.includes('ADMIN')) {
            this.disabled = false;
        }
        // Set the default
        this.clinic = new Clinic();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to update product on changes
        this._clinicService.onProductChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(clinic => {

                if (clinic) {
                    this.clinic = new Clinic(clinic);
                    this.pageType = 'edit';
                } else {
                    this.pageType = 'new';
                    this.clinic = new Clinic();
                }

                this.doctorForm = this.createClinicForm();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create product form
     *
     * @returns {FormGroup}
     */
    createClinicForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.clinic.id],
            nome: [this.clinic.nome],
            morada: [this.clinic.morada],
            contacto: [this.clinic.contacto]
        });
    }

    /**
     * Save product
     */
    saveClinic(): void {
        const data = this.doctorForm.getRawValue();

        this._clinicService.saveClinic(data)
            .then(() => {

                // Trigger the subscription with new data
                this._clinicService.onProductChanged.next(data);

                // Show the success message
                this._matSnackBar.open('Clinica alterada com sucesso.', 'OK', {
                    verticalPosition: 'top',
                    duration: 2000
                });

                this._router.navigate(['list/clinics']);
            });
    }

    /**
     * Add product
     */
    addClinic(): void {
        const data = this.doctorForm.getRawValue();

        this._clinicService.addClinic(data)
            .then(() => {

                // Trigger the subscription with new data
                this._clinicService.onProductChanged.next(data);

                // Show the success message
                this._matSnackBar.open('Clinica adicionada', 'OK', {
                    verticalPosition: 'top',
                    duration: 2000
                });
                this._router.navigate(['list/clinics']);
            });
    }

    deleteDoctor(): void {
        const data = this.doctorForm.getRawValue();

        this._clinicService.deleteDoctor(data)
            .then(() => {

                // Trigger the subscription with new data
                this._clinicService.onProductChanged.next(data);

                // Show the success message
                this._matSnackBar.open('Clinica apagada com sucesso.', 'OK', {
                    verticalPosition: 'top',
                    duration: 2000
                });
                this._router.navigate(['list/clinics']);
            });

    }
}
