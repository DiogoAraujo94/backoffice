import {FuseUtils} from '@fuse/utils';

export class Clinic {
    id: string;
    nome: string;
    morada: string;
    contacto: string;


    /**
     * Constructor
     *
     * @param product
     */
    constructor(clinic?) {
        clinic = clinic || {};
        this.id = clinic.id || FuseUtils.generateGUID();
        this.nome = clinic.nome || '';
        this.morada = clinic.morada || '';
        this.contacto = clinic.contacto || '';
    }

}
