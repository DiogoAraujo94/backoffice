import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import {environment} from '../../../../environments/environment';

@Injectable()
export class ClinicService implements Resolve<any>
{
    routeParams: any;
    doctor: any;
    onProductChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    )
    {
        // Set the defaults
        this.onProductChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any
    {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getClinic()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get product
     *
     * @returns {Promise<any>}
     */
    getClinic(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            if ( this.routeParams.id === 'new' )
            {
                this.onProductChanged.next(false);
                resolve(false);
            }
            else
            {
                this._httpClient.get(environment.apiUrl + 'clinics/' + this.routeParams.id)
                    .subscribe((response: any) => {
                        this.doctor = response.data[0];
                        this.onProductChanged.next(this.doctor);
                        resolve(response);
                    }, reject);
            }
        });
    }

    /**
     * Save patient
     *
     * @param patient
     * @returns {Promise<any>}
     */
    saveClinic(clinic): Promise<any>
    {

        return new Promise((resolve, reject) => {
            const headers = {
                'Authorization': 'Bearer my-token',
                'Clinica': clinic.id.toString(),
                'Nome': clinic.nome,
                'Morada': clinic.morada,
                'Contacto': clinic.contacto
            };
            const body = {title: 'Angular POST Request Example'};
            console.log('headers', headers);
            this._httpClient.post(environment.apiUrl + 'clinics/update', body, {headers})
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Add product
     *
     * @param product
     * @returns {Promise<any>}
     */
    addClinic(clinic): Promise<any>
    {
        return new Promise((resolve, reject) => {
            const headers = {
                'Authorization': 'Bearer my-token',
                'Nome': clinic.nome,
                'Morada': clinic.morada,
                'Contacto': clinic.contacto

            };
            const body = {title: 'Angular POST Request Example'};
            console.log('headers', headers);
            this._httpClient.post(environment.apiUrl + 'clinics', body, {headers})
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    deleteDoctor(clinic): Promise<any>
    {
        return new Promise((resolve, reject) => {
            const headers = {
                'Authorization': 'Bearer my-token',
                'Id': clinic.id.toString()
            };
            const body = {title: 'Angular POST Request Example'};
            console.log('headers', headers);
            this._httpClient.post(environment.apiUrl + 'clinics/delete', body, {headers})
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
}
