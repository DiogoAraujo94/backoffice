import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Location} from '@angular/common';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';

import {DoctorService} from './doctor.service';
import {Doctor} from './doctor.model';
import {Router} from '@angular/router';
import {UserService} from '../../login/user.service';
import {ClinicsService} from '../clinics/clinics.service';

@Component({
    selector: 'app-products',
    templateUrl: './doctor.component.html',
    styleUrls: ['./doctor.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class DoctorComponent implements OnInit, OnDestroy {
    doctor: Doctor;
    pageType: string;
    doctorForm: FormGroup;
    especialidades: any = ['Ortodontia', 'Endodontia', 'Odontopediatria', 'Generalista', 'Implantologia', 'Prótese Fixa','Estética e Reabilitação', 'Cirurgia', 'Outras especialidades', 'Urgencia', 'Consulta Avaliação'];

    // Private
    private _unsubscribeAll: Subject<any>;
    private user: any;
    disabled: boolean;
    clinics: any[] = [];

    /**
     * Constructor
     *
     * @param doctorService
     * @param {FormBuilder} _formBuilder
     * @param {Location} _location
     * @param _router
     * @param {MatSnackBar} _matSnackBar
     */
    constructor(
        private doctorService: DoctorService,
        private _formBuilder: FormBuilder,
        private _location: Location,
        private _matSnackBar: MatSnackBar,
        private _userService: UserService,
        private _clinicsService: ClinicsService,
        private _router: Router
    ) {

        if (this._userService.getUser() === undefined) {
            this._router.navigate(['login'], {replaceUrl: true});
        }
        this._clinicsService.getClinics().then(data =>
           data.forEach(clinic => this.clinics.push(clinic.nome))
       );
        this.user = this._userService.getUser();
        this.user.credenciais === 'USER' ? this.disabled = true : this.disabled = false;
        if (this.user.credenciais.includes('ADMIN')) {
            this.disabled = false;
        }
        // Set the default
        this.doctor = new Doctor();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to update product on changes
        this.doctorService.onProductChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(doctor => {

                if (doctor) {
                    this.doctor = new Doctor(doctor);
                    this.pageType = 'edit';
                } else {
                    this.pageType = 'new';
                    this.doctor = new Doctor();
                }

                this.doctorForm = this.createDoctorForm();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Create product form
     *
     * @returns {FormGroup}
     */
    createDoctorForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.doctor.id],
            nome: [this.doctor.nome],
            fotografia: [],
            especialidade: [this.doctor.especialidade],
            clinica: [this.doctor.clinica],
        });
    }

    /**
     * Save product
     */
    saveDoctor(): void {
        const data = this.doctorForm.getRawValue();

        this.doctorService.saveDoctor(data)
            .then(() => {

                // Trigger the subscription with new data
                this.doctorService.onProductChanged.next(data);

                // Show the success message
                this._matSnackBar.open('Medico alterado com sucesso.', 'OK', {
                    verticalPosition: 'top',
                    duration: 2000
                });

                this._router.navigate(['list/doctors']);
            });
    }

    /**
     * Add product
     */
    addDoctor(): void {
        const data = this.doctorForm.getRawValue();

        this.doctorService.addDoctor(data)
            .then(() => {

                // Trigger the subscription with new data
                this.doctorService.onProductChanged.next(data);

                // Show the success message
                this._matSnackBar.open('Medico adicionado', 'OK', {
                    verticalPosition: 'top',
                    duration: 2000
                });
                this._router.navigate(['list/doctors']);


            });
    }

    deleteDoctor(): void {
        const data = this.doctorForm.getRawValue();

        this.doctorService.deleteDoctor(data)
            .then(() => {

                // Trigger the subscription with new data
                this.doctorService.onProductChanged.next(data);

                // Show the success message
                this._matSnackBar.open('Medico apagado com sucesso.', 'OK', {
                    verticalPosition: 'top',
                    duration: 2000
                });
                this._router.navigate(['list/doctors']);
            });

    }

    handleEspecialidade(especialidade: any): void {
        this.doctorForm.patchValue({especialidade: especialidade});

    }

    handleClinica(clinica: any): void {

        this.doctorForm.patchValue({clinica: clinica});

    }
}
