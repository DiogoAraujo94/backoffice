import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';

@Injectable()
export class DoctorService implements Resolve<any> {
    routeParams: any;
    doctor: any;
    onProductChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.onProductChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getDoctor()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get product
     *
     * @returns {Promise<any>}
     */
    getDoctor(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.routeParams.id === 'new') {
                this.onProductChanged.next(false);
                resolve(false);
            } else {
                this._httpClient.get(environment.apiUrl + 'doctors/' + this.routeParams.id)
                    .subscribe((response: any) => {
                        this.doctor = response.data[0];
                        this.onProductChanged.next(this.doctor);
                        resolve(response);
                    }, reject);
            }
        });
    }

    /**
     * Save patient
     *
     * @param patient
     * @returns {Promise<any>}
     */
    async saveDoctor(doctor): Promise<any> {
        let fotografia = '';
        if (doctor.fotografia) {
            const file = doctor.fotografia.files[0];

            const formData = new FormData();
            formData.append('file', file);
            const res: any = await this._httpClient.post(environment.apiUrl + 'file/upload', formData, {}).toPromise();
            fotografia = res.file;
            const headers = {
                'Doutor': doctor.id.toString(),
                'Fotografia': fotografia,
            };
            const body = {title: 'Angular POST Request Example'};

            const foto = await new Promise((resolve, reject) => {
                console.log('foto:', headers);

                this._httpClient.post(environment.apiUrl + 'doctors/photo', body,{headers}).subscribe((response: any) => {
                    resolve(response);
                }, reject);
            });
        }
        return new Promise((resolve, reject) => {
            const headers = {
                'Authorization': 'Bearer my-token',
                'Doutor': doctor.id.toString(),
                'Nome': doctor.nome,
                'Fotografia': fotografia,
                'Especialidade': doctor.especialidade,
                'Clinica': doctor.clinica
            };
            const body = {title: 'Angular POST Request Example'};
            console.log('headers', headers);
            this._httpClient.post(environment.apiUrl + 'doctors/update', body, {headers})
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Add product
     *
     * @param product
     * @returns {Promise<any>}
     */
    async addDoctor(doctor): Promise<any> {
        let fotografia = '';
        if (doctor.fotografia) {
            const file = doctor.fotografia.files[0];

            const formData = new FormData();
            formData.append('file', file);
            const res: any = await this._httpClient.post(environment.apiUrl + 'file/upload', formData, {}).toPromise();
            fotografia = res.file;
        }
        return new Promise((resolve, reject) => {
            const headers = {
                'Authorization': 'Bearer my-token',
                'Nome': doctor.nome,
                'Fotografia': fotografia,
                'Especialidade': doctor.especialidade,
                'Clinica': doctor.clinica
            };
            const body = {title: 'Angular POST Request Example'};
            console.log('headers', headers);
            this._httpClient.post(environment.apiUrl + 'doctors', body, {headers})
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    deleteDoctor(doctor): Promise<any> {
        return new Promise((resolve, reject) => {
            const headers = {
                'Authorization': 'Bearer my-token',
                'Id': doctor.id.toString()
            };
            const body = {title: 'Angular POST Request Example'};
            console.log('headers', headers);
            this._httpClient.post(environment.apiUrl + 'doctors/delete', body, {headers})
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
}
