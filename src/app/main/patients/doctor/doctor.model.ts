import {MatChipInputEvent} from '@angular/material/chips';

import {FuseUtils} from '@fuse/utils';

export class Doctor {
    id: string;
    nome: string;
    fotografia: string;
    especialidade: string;
    clinica: string;


    /**
     * Constructor
     *
     * @param product
     */
    constructor(doctor?) {
        doctor = doctor || {};
        this.id = doctor.id || FuseUtils.generateGUID();
        this.nome = doctor.nome || '';
        this.fotografia = doctor.fotografia || '';
        this.especialidade = doctor.especialidade || '';
        this.clinica = doctor.clinica || '';
    }

}
