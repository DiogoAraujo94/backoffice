import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {UserService} from '../../login/user.service';

@Injectable()
export class ClinicsService implements Resolve<any> {
    clinics: any[];
    onProductsChanged: BehaviorSubject<any>;
    private user: any;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _userService: UserService
    ) {
        this.user = this._userService.getUser();
        // Set the defaults
        this.onProductsChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getClinics()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get products
     *
     * @returns {Promise<any>}
     */
    getClinics(): Promise<any> {
        const headers = {
            'Authorization': 'Bearer my-token',
            'User': this.user.credenciais,
            clinica: this.user.clinica ? this.user.clinica : '',
        };
        return new Promise((resolve, reject) => {
            this._httpClient.get(environment.apiUrl + 'clinics', {headers})
                .subscribe((response: any) => {
                    this.clinics = response.data;
                    this.onProductsChanged.next(this.clinics);
                    resolve(response.data);
                }, reject);
        });
    }
}
