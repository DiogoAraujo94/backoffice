import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {DataSource} from '@angular/cdk/collections';
import {BehaviorSubject, fromEvent, merge, Observable, Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged, map, takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';
import {FuseUtils} from '@fuse/utils';
import {DoctorsService} from './doctors.service';
import {UserService} from '../../login/user.service';
import {Router} from '@angular/router';

@Component({
    selector: 'e-commerce-products',
    templateUrl: './doctors.component.html',
    styleUrls: ['./doctors.component.scss'],
    animations: fuseAnimations,
    encapsulation: ViewEncapsulation.None
})
export class DoctorsComponent implements OnInit {
    dataSource: FilesDataSource | null;
    displayedColumns = ['id', 'fotografia', 'nome', 'especialidade'];

    @ViewChild(MatPaginator, {static: true})
    paginator: MatPaginator;

    @ViewChild(MatSort, {static: true})
    sort: MatSort;

    @ViewChild('filter', {static: true})
    filter: ElementRef;

    // Private
    private _unsubscribeAll: Subject<any>;
    disabled: boolean;
    private user: any;

    constructor(
        private _ecommerceProductsService: DoctorsService,
        private _userService: UserService,
        private _router: Router
    ) {

        if (this._userService.getUser() === undefined) {
            this._router.navigate(['login'], {replaceUrl: true});
        }
        this.user = this._userService.getUser();
        if (this.user.credenciais === 'USER') {
            this._router.navigate(['/list/proposals']);
        }
        this.user.credenciais === 'USER' || 'GESTOR' ? this.disabled = true : this.disabled = false;
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.dataSource = new FilesDataSource(this._ecommerceProductsService, this.paginator, this.sort);

        fromEvent(this.filter.nativeElement, 'keyup')
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(150),
                distinctUntilChanged()
            )
            .subscribe(() => {
                if (!this.dataSource) {
                    return;
                }

                this.dataSource.filter = this.filter.nativeElement.value;
            });
    }

    navigateToDoctor(id: any): void {
        if (this.disabled) {
            this._router.navigate(['/list/doctors/' + id.toString() + '/edit']);
        }
    }
}

export class FilesDataSource extends DataSource<any> {
    private _filterChange = new BehaviorSubject('');
    private _filteredDataChange = new BehaviorSubject('');

    /**
     * Constructor
     *
     * @param {EcommerceProductsService} _ecommerceProductsService
     * @param {MatPaginator} _matPaginator
     * @param {MatSort} _matSort
     */
    constructor(
        private _ecommerceProductsService: DoctorsService,
        private _matPaginator: MatPaginator,
        private _matSort: MatSort
    ) {
        super();

        this.filteredData = this._ecommerceProductsService.doctors;
    }

    /**
     * Connect function called by the table to retrieve one stream containing the data to render.
     *
     * @returns {Observable<any[]>}
     */
    connect(): Observable<any[]> {
        const displayDataChanges = [
            this._ecommerceProductsService.onProductsChanged,
            this._matPaginator.page,
            this._filterChange,
            this._matSort.sortChange
        ];

        return merge(...displayDataChanges)
            .pipe(
                map(() => {
                        let data = this._ecommerceProductsService.doctors.slice();

                        data = this.filterData(data);

                        this.filteredData = [...data];

                        data = this.sortData(data);

                        // Grab the page's slice of data.
                        const startIndex = this._matPaginator.pageIndex * this._matPaginator.pageSize;
                        return data.splice(startIndex, this._matPaginator.pageSize);
                    }
                ));
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    // Filtered data
    get filteredData(): any {
        return this._filteredDataChange.value;
    }

    set filteredData(value: any) {
        this._filteredDataChange.next(value);
    }

    // Filter
    get filter(): string {
        return this._filterChange.value;
    }

    set filter(filter: string) {
        this._filterChange.next(filter);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Filter data
     *
     * @param data
     * @returns {any}
     */
    filterData(data): any {
        if (!this.filter) {
            return data;
        }
        return FuseUtils.filterArrayByString(data, this.filter);
    }

    /**
     * Sort data
     *
     * @param data
     * @returns {any[]}
     */
    sortData(data): any[] {
        if (!this._matSort.active || this._matSort.direction === '') {
            return data;
        }

        return data.sort((a, b) => {
            let propertyA: number | string = '';
            let propertyB: number | string = '';

            switch (this._matSort.active) {
                case 'id':
                    [propertyA, propertyB] = [a.id, b.id];
                    break;
                case 'name':
                    [propertyA, propertyB] = [a.name, b.name];
                    break;
                case 'categories':
                    [propertyA, propertyB] = [a.categories[0], b.categories[0]];
                    break;
                case 'price':
                    [propertyA, propertyB] = [a.priceTaxIncl, b.priceTaxIncl];
                    break;
                case 'quantity':
                    [propertyA, propertyB] = [a.quantity, b.quantity];
                    break;
                case 'active':
                    [propertyA, propertyB] = [a.active, b.active];
                    break;
            }

            const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
            const valueB = isNaN(+propertyB) ? propertyB : +propertyB;

            return (valueA < valueB ? -1 : 1) * (this._matSort.direction === 'asc' ? 1 : -1);
        });
    }

    /**
     * Disconnect
     */
    disconnect(): void {
    }
}


// // import {Component, ElementRef, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
// // import {MatPaginator} from '@angular/material/paginator';
// // import {MatSort} from '@angular/material/sort';
// // import {DataSource} from '@angular/cdk/collections';
// // import {BehaviorSubject, fromEvent, merge, Observable, Subject} from 'rxjs';
// // import {debounceTime, distinctUntilChanged, map} from 'rxjs/operators';
// //
// // import {fuseAnimations} from '@fuse/animations';
// // import {FuseUtils} from '@fuse/utils';
// //
// // import {takeUntil} from 'rxjs/operators';
// // import {DoctorsService} from './products.service';
// //
// // @Component({
// //     selector: 'app-products',
// //     templateUrl: './doctors.component.html',
// //     styleUrls: ['./doctors.component.scss']
// // })
// // export class DoctorsComponent implements OnInit {
// //     dataSource: FilesDataSource | null;
// //     displayedColumns = ['id', 'name'];
// //
// //     @ViewChild(MatPaginator, {static: true})
// //     paginator: MatPaginator;
// //
// //     @ViewChild(MatSort, {static: true})
// //     sort: MatSort;
// //
// //     @ViewChild('filter', {static: true})
// //     filter: ElementRef;
// //
// //     // Private
// //     private _unsubscribeAll: Subject<any>;
// //
// //     constructor(
// //         private _ecommerceProductsService: DoctorsService
// //     ) {
// //         // Set the private defaults
// //         this._unsubscribeAll = new Subject();
// //     }
// //
// //     // -----------------------------------------------------------------------------------------------------
// //     // @ Lifecycle hooks
// //     // -----------------------------------------------------------------------------------------------------
// //
// //     /**
// //      * On init
// //      */
// //     ngOnInit(): void {
// //         this.dataSource = new FilesDataSource(this._ecommerceProductsService, this.paginator, this.sort);
// //
// //         fromEvent(this.filter.nativeElement, 'keyup')
// //             .pipe(
// //                 takeUntil(this._unsubscribeAll),
// //                 debounceTime(150),
// //                 distinctUntilChanged()
// //             )
// //             .subscribe(() => {
// //                 if (!this.dataSource) {
// //                     return;
// //                 }
// //
// //                 this.dataSource.filter = this.filter.nativeElement.value;
// //             });
// //     }
// // }
// //
// // export class FilesDataSource extends DataSource<any> {
// //     private _filterChange = new BehaviorSubject('');
// //     private _filteredDataChange = new BehaviorSubject('');
// //
// //     /**
// //      * Constructor
// //      *
// //      * @param {EcommerceProductsService} _ecommerceProductsService
// //      * @param {MatPaginator} _matPaginator
// //      * @param {MatSort} _matSort
// //      */
// //     constructor(
// //         private _ecommerceProductsService: DoctorsService,
// //         private _matPaginator: MatPaginator,
// //         private _matSort: MatSort
// //     ) {
// //         super();
// //
// //         this.filteredData = this._ecommerceProductsService.getProducts();
// //     }
// //
// //     /**
// //      * Connect function called by the table to retrieve one stream containing the data to render.
// //      *
// //      * @returns {Observable<any[]>}
// //      */
// //     connect(): Observable<any[]> {
// //         const displayDataChanges = [
// //             this._ecommerceProductsService.onProductsChanged,
// //             //  this._matPaginator.page,
// //             this._filterChange,
// //             this._matSort.sortChange
// //         ];
// //
// //         return merge(...displayDataChanges)
// //             .pipe(
// //                 map(() => {
// //                         let data =  this._ecommerceProductsService.getProducts().then(
// //                             () => {
// //                               let data2 = this._ecommerceProductsService.products.slice();
// //                               return data2;
// //                             }
// //                         );
// //
// //                         data = this.filterData(data);
// //
// //                         this.filteredData = [...data.];
// //
// //                         data = this.sortData(data);
// //
// //                         // Grab the page's slice of data.
// //                         const startIndex = this._matPaginator.pageIndex * this._matPaginator.pageSize;
// //                         return data.splice(startIndex, this._matPaginator.pageSize);
// //                     }
// //                 ));
// //     }
// //
// //     // -----------------------------------------------------------------------------------------------------
// //     // @ Accessors
// //     // -----------------------------------------------------------------------------------------------------
// //
// //     // Filtered data
// //     get filteredData(): any {
// //         return this._filteredDataChange.value;
// //     }
// //
// //     set filteredData(value: any) {
// //         this._filteredDataChange.next(value);
// //     }
// //
// //     // Filter
// //     get filter(): string {
// //         return this._filterChange.value;
// //     }
// //
// //     set filter(filter: string) {
// //         this._filterChange.next(filter);
// //     }
// //
// //     // -----------------------------------------------------------------------------------------------------
// //     // @ Public methods
// //     // -----------------------------------------------------------------------------------------------------
// //
// //     /**
// //      * Filter data
// //      *
// //      * @param data
// //      * @returns {any}
// //      */
// //     filterData(data): any {
// //         if (!this.filter) {
// //             return data;
// //         }
// //         return FuseUtils.filterArrayByString(data, this.filter);
// //     }
// //
// //     /**
// //      * Sort data
// //      *
// //      * @param data
// //      * @returns {any[]}
// //      */
// //     sortData(data): any[] {
// //         if (!this._matSort.active || this._matSort.direction === '') {
// //             return data;
// //         }
// //
// //         return data.sort((a, b) => {
// //             let propertyA: number | string = '';
// //             let propertyB: number | string = '';
// //
// //             switch (this._matSort.active) {
// //                 case 'id':
// //                     [propertyA, propertyB] = [a.id, b.id];
// //                     break;
// //                 case 'name':
// //                     [propertyA, propertyB] = [a.name, b.name];
// //                     break;
// //                 case 'categories':
// //                     [propertyA, propertyB] = [a.categories[0], b.categories[0]];
// //                     break;
// //                 case 'price':
// //                     [propertyA, propertyB] = [a.priceTaxIncl, b.priceTaxIncl];
// //                     break;
// //                 case 'quantity':
// //                     [propertyA, propertyB] = [a.quantity, b.quantity];
// //                     break;
// //                 case 'active':
// //                     [propertyA, propertyB] = [a.active, b.active];
// //                     break;
// //             }
// //
// //             const valueA = isNaN(+propertyA) ? propertyA : +propertyA;
// //             const valueB = isNaN(+propertyB) ? propertyB : +propertyB;
// //
// //             return (valueA < valueB ? -1 : 1) * (this._matSort.direction === 'asc' ? 1 : -1);
// //         });
// //     }
// //
// //     /**
// //      * Disconnect
// //      */
// //     disconnect(): void {
// //     }
// // }
// //
//
// import {Component, OnDestroy, OnInit} from '@angular/core';
// import {Subject} from 'rxjs';
// import {MatDialog} from '@angular/material/dialog';
// import {HttpClient} from '@angular/common/http';
// import {environment} from '../../../../environments/environment';
// import {CreatePatientDialogComponent} from '../create-patient-dialog/create-patient-dialog.component';
// import {FormGroup} from '@angular/forms';
// import {CreateProductDialogComponent} from '../create-product-dialog/create-product-dialog.component';
//
// @Component({
//   selector: 'app-products',
//   templateUrl: './doctors.component.html',
//   styleUrls: ['./doctors.component.scss']
// })
// export class DoctorsComponent implements OnInit, OnDestroy{
//     rows: any[];
//     loadingIndicator: boolean;
//     dialogRef: any;
//     reorderable: boolean;
//     collapsed: boolean;
//
//
//     // Private
//     private _unsubscribeAll: Subject<any>;
//
//     /**
//      * Constructor
//      *
//      * @param {HttpClient} _httpClient
//      */
//     constructor(
//         private _matDialog: MatDialog,
//         private _httpClient: HttpClient
//     ) {
//         // Set the defaults
//         this.loadingIndicator = true;
//         this.reorderable = true;
//         this.collapsed = true;
//
//
//         // Set the private defaults
//         this._unsubscribeAll = new Subject();
//     }
//
//     // -----------------------------------------------------------------------------------------------------
//     // @ Lifecycle hooks
//     // -----------------------------------------------------------------------------------------------------
//     public selectedRows = [];
//     searchTerm: any;
//
//
//     /**
//      * On init
//      */
//     ngOnInit(): void {
//         this._httpClient.get(environment.apiUrl + 'products')
//             .subscribe((products: any) => {
//                 console.log('Products: ', products);
//                 this.rows = products.data;
//                 this.loadingIndicator = false;
//             });
//     }
//
//     /**
//      * On destroy
//      */
//     ngOnDestroy(): void {
//         // Unsubscribe from all subscriptions
//         this._unsubscribeAll.next();
//         this._unsubscribeAll.complete();
//     }
//
//     /**
//      * Compose dialog
//      */
//     composeDialog(): void {
//         this.dialogRef = this._matDialog.open(CreateProductDialogComponent, {
//             width: '50%',
//             height: '80%'
//         });
//
//         this.dialogRef.afterClosed()
//             .subscribe(response => {
//                 if (!response) {
//                     return;
//                 }
//                 const actionType: string = response[0];
//                 const formData: FormGroup = response[1];
//                 switch (actionType) {
//                     /**
//                      * Send
//                      */
//                     case 'send':
//                         console.log('new Product', formData.getRawValue());
//                         console.log('new Product', formData.getRawValue().nome);
//                         const headers = {
//                             'Authorization': 'Bearer my-token',
//                             'Codigo': formData.getRawValue().codigo,
//                             'Nome': formData.getRawValue().nome,
//                             'Descricao': formData.getRawValue().descricao,
//                             'Especialidade': formData.getRawValue().especialidade,
//                             'Preco': formData.getRawValue().preco
//                         };
//                         const body = {title: 'Angular POST Request Example'};
//                         this._httpClient.post(environment.apiUrl + 'products', body, {headers}).subscribe(data => {
//                             this.selectedRows = [];
//                             this._httpClient.get(environment.apiUrl + 'products')
//                                 .subscribe((products: any) => {
//                                     console.log('Products: ', products);
//                                     this.rows = products.data;
//                                     this.loadingIndicator = false;
//                                 });
//                             console.log('post data: ', data);
//                         });
//                         break;
//                     /**
//                      * Delete
//                      */
//                     case 'delete':
//                         console.log('delete Mail');
//                         break;
//                 }
//             });
//     }
//
//     handleTableSelection(selected: any): void {
//         this.selectedRows = selected;
//         console.log('selected', selected);
//     }
//
//     openEditDialog(): void {
//         this.dialogRef = this._matDialog.open(CreateProductDialogComponent, {
//             width: '50%',
//             height: '80%',
//             data: this.selectedRows[0]
//         });
//
//         this.dialogRef.afterClosed()
//             .subscribe(response => {
//                 if (!response) {
//                     return;
//                 }
//                 const actionType: string = response[0];
//                 const formData: FormGroup = response[1];
//                 switch (actionType) {
//                     /**
//                      * Send
//                      */
//                     case 'send':
//                         console.log('new Patient', formData.getRawValue());
//                         console.log('new Patient', formData.getRawValue().nome);
//                         const headers = {
//                             'Authorization': 'Bearer my-token',
//                             'Paciente': this.selectedRows[0].id.toString(),
//                             'Codigo': formData.getRawValue().codigo,
//                             'Nome': formData.getRawValue().nome,
//                             'Descricao': formData.getRawValue().descricao,
//                             'Especialidade': formData.getRawValue().especialidade,
//                             'Preco': formData.getRawValue().preco
//                         };
//                         const body = {title: 'Angular POST Request Example'};
//                         this._httpClient.post(environment.apiUrl + 'products/update', body, {headers}).subscribe(data => {
//                             console.log('post data: ', data);
//                             this.selectedRows = [];
//                             this._httpClient.get(environment.apiUrl + 'products')
//                                 .subscribe((products: any) => {
//                                     console.log('Products: ', products);
//                                     this.rows = products.data;
//                                     this.loadingIndicator = false;
//                                 });
//                         });
//
//                         break;
//                     /**
//                      * Delete
//                      */
//                     case 'delete':
//                         console.log('delete Mail');
//                         break;
//                 }
//             });
//
//
//     }
//
//     deleteSelectedProduct(): void {
//         const headers = {
//             'Authorization': 'Bearer my-token',
//             'Id': this.selectedRows[0].id.toString()
//         };
//
//         const body = {title: 'Angular POST Request Example'};
//         this._httpClient.post(environment.apiUrl + 'products/delete', body, {headers}).subscribe(data => {
//             console.log('post data: ', data);
//             this.selectedRows = [];
//             this._httpClient.get(environment.apiUrl + 'products')
//                 .subscribe((products: any) => {
//                     console.log('Products: ', products);
//                     this.rows = products.data;
//                     this.loadingIndicator = false;
//                 });
//         });
//     }
//
//     /**
//      * Filter Patients by term
//      */
//     filterProductsByTerm(): void {
//         const searchTerm = this.searchTerm.toLowerCase();
//
//         // Search
//         if (searchTerm === '') {
//             this._httpClient.get(environment.apiUrl + 'products')
//                 .subscribe((products: any) => {
//                     console.log('Products: ', products);
//                     this.rows = products.data;
//                     this.loadingIndicator = false;
//                 });
//         } else {
//             this.rows = this.rows.filter((patient) => {
//                 return patient.nome.toLowerCase().includes(searchTerm);
//             });
//         }
//     }
//
//
//     /**
//      * Collapse
//      */
//     collapse(): void
//     {
//         this.collapsed = true;
//     }
//
//     /**
//      * Expand
//      */
//     expand(): void
//     {
//         this.collapsed = false;
//     }
// }
