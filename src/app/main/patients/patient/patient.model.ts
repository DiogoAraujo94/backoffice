import {MatChipInputEvent} from '@angular/material/chips';

import {FuseUtils} from '@fuse/utils';

export class Patient {
    id: string;
    nome: string;
    contacto: string;
    genero: string;
    cidade: string;
    valor: string;
    email: string;
    morada: string;
    postal: string;
    pais: string;
    nascimento: Date | string;
    seguro: string;
    nif: string;
    fuma: any;
    cigarros: any;
    gravida: any;
    estomago: any;
    detalhe: any;
    halito: any;
    sensibilidade: any;
    gengivas: any;
    dentes: any;
    fio: any;
    ultima: any;
    queixas: any;
    tratamentos: any;
    alergias: any;
    medicacaoescolha: any;
    medicacao: any;
    coracaoescolha: any;
    coracao: any;
    infectaescolha: any;
    infecta: any;
    alteracoes: any;
    motivacao: any;
    sorriso: any;
    recomendacao: any;
    seguradora: any;
    profissao: any;

    /**
     * Constructor
     *
     * @param patient
     */
    constructor(patient?) {
        patient = patient || {};
        this.id = patient.id || FuseUtils.generateGUID();
        this.nome = patient.nome || '';
        this.contacto = patient.contacto || '';
        this.genero = patient.genero || '';
        this.cidade = patient.cidade || '';
        this.valor = patient.valor || '';
        this.email = patient.email || '';
        this.morada = patient.morada || '';
        this.postal = patient.postal || '';
        this.pais = patient.pais || '';
        this.nascimento = new Date(patient.nascimento) || '';
        this.seguro = patient.seguro || '';
        this.nif = patient.nif || '';
        this.fuma = patient.fuma || '';
        this.cigarros = patient.cigarros || '';
        this.gravida = patient.gravida || '';
        this.estomago = patient.estomago || '';
        this.detalhe = patient.detalhe || '';
        this.halito = patient.halito || '';
        this.sensibilidade = patient.sensibilidade || '';
        this.gengivas = patient.gengivas || '';
        this.dentes = patient.dentes || '';
        this.fio = patient.fio || '';
        this.ultima = patient.ultima || '';
        this.queixas = patient.queixas || '';
        this.tratamentos = patient.tratamentos || '';
        this.alergias = patient.alergias || '';
        this.medicacaoescolha = patient.medicacaoescolha || '';
        this.medicacao = patient.medicacao || '';
        this.coracaoescolha = patient.coracaoescolha || '';
        this.coracao = patient.coracao || '';
        this.infectaescolha = patient.infectaescolha || '';
        this.infecta = patient.infecta || '';
        this.alteracoes = patient.alteracoes || '';
        this.motivacao = patient.motivacao || '';
        this.sorriso = patient.sorriso || '';
        this.recomendacao = patient.recomendacao || '';
        this.seguradora = patient.seguradora || '';
        this.profissao = patient.profissao || '';

    }

}
