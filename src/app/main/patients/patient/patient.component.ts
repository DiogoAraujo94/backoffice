import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {Location} from '@angular/common';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {fuseAnimations} from '@fuse/animations';

import {PatientService} from './patient.service';
import {Patient} from './patient.model';
import {Router} from '@angular/router';
import {UserService} from '../../login/user.service';
import {MatCheckboxChange} from '@angular/material/checkbox';

@Component({
    selector: 'app-products',
    templateUrl: './patient.component.html',
    styleUrls: ['./patient.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class PatientComponent implements OnInit, OnDestroy {
    public seguradora: any;

    /**
     * Constructor
     *
     * @param {EcommerceProductService} patientService
     * @param {FormBuilder} _formBuilder
     * @param {Location} _location
     * @param {MatSnackBar} _matSnackBar
     */
    constructor(
        private patientService: PatientService,
        private _formBuilder: FormBuilder,
        private _location: Location,
        private _matSnackBar: MatSnackBar,
        private _userService: UserService,
        private _router: Router
    ) {

        if (this._userService.getUser() === undefined) {
            this._router.navigate(['login'], {replaceUrl: true});
        }
        this.user = this._userService.getUser();
        this.user.credenciais === 'USER' || 'GESTOR' ? this.disabled = true : this.disabled = false;
        if (this.user.credenciais.includes('ADMIN')) {
            this.disabled = false;
        }
        // Set the default
        this.patient = new Patient();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    patient: Patient;
    pageType: string;
    patientForm: FormGroup;

    // Private
    private _unsubscribeAll: Subject<any>;
    private user: any;
    disabled: boolean;

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    selectedValue: any;
    generos: any = ['Masculino', 'Feminino'];
    simOuNao: any = ['Sim', 'Não'];
    alteracoes: any = ['Cancro', 'Diabetes', 'Colesterol', 'Depressão', 'Nenhuma'];
    alergias: any = ['Anestesia', 'Iodo', 'Sulfamidas', 'Codeína', 'Penicilina', 'Aspirina', 'Calmantes', 'Outros'];
    fuma: any;
    recomendacoes: any = ['Seguradora', 'Redes Sociais', 'Google', 'Recomendação', 'Outdoors', 'Flyers', 'Publicidade/Anuncios', 'Passagem pela loja', 'Turismo', 'Trade'];
    seguradoras: any = ['Não tem', 'Saude Particular', 'Adeland', 'Future Healthcare', 'Medicare', 'Advancecare'];

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to update product on changes
        this.patientService.onProductChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(patient => {

                if (patient) {
                    this.patient = new Patient(patient);
                    this.patientForm = this.createPatientForm();
                    this.seguradora = this.patient.seguradora;
                    console.log(patient);
                    const alergias = JSON.parse(this.patient.alergias);
                    const alergiasForm = this.patientForm.get('alergias') as FormArray;
                    console.log('alergiasFormCOnst: ', alergiasForm);
                    alergias.forEach(prod => {
                        console.log(prod);
                        alergiasForm.push(new FormControl(prod.toString()));
                    });
                    const alteracoes = JSON.parse(this.patient.alteracoes);
                    const alteracoesForm = this.patientForm.get('alteracoes') as FormArray;
                    alteracoes.forEach(prod => alteracoesForm.push(new FormControl(prod.toString())));
                    this.pageType = 'edit';
                } else {
                    this.pageType = 'new';
                    this.patient = new Patient();
                    this.patientForm = this.createPatientForm();


                }
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    /**
     * Create product form
     *
     * @returns {FormGroup}
     */
    createPatientForm(): FormGroup {
        return this._formBuilder.group({
            id: [this.patient.id],
            nome: [this.patient.nome],
            contacto: [this.patient.contacto],
            genero: [this.patient.genero],
            cidade: [this.patient.cidade],
            valor: [this.patient.valor],
            email: [this.patient.email],
            morada: [this.patient.morada],
            pais: [this.patient.pais],
            postal: [this.patient.postal],
            nascimento: [this.patient.nascimento],
            seguro: [this.patient.seguro],
            nif: [this.patient.nif],
            fuma: [this.patient.fuma],
            cigarros: [this.patient.cigarros],
            gravida: [this.patient.gravida],
            estomago: [this.patient.estomago],
            detalhe: [this.patient.detalhe],
            halito: [this.patient.halito],
            sensibilidade: [this.patient.sensibilidade],
            gengivas: [this.patient.gengivas],
            dentes: [this.patient.dentes],
            fio: [this.patient.fio],
            ultima: [this.patient.ultima],
            queixas: [this.patient.queixas],
            tratamentos: [this.patient.tratamentos],
            alergias: this._formBuilder.array([]),
            medicacaoescolha: [this.patient.medicacaoescolha],
            medicacao: [this.patient.medicacao],
            coracaoescolha: [this.patient.coracaoescolha],
            coracao: [this.patient.coracao],
            infectaescolha: [this.patient.infectaescolha],
            infecta: [this.patient.infecta],
            alteracoes: this._formBuilder.array([]),
            motivacao: [this.patient.motivacao],
            sorriso: [this.patient.sorriso],
            recomendacao: [this.patient.recomendacao],
            seguradora: [this.patient.seguradora],
            profissao: [this.patient.profissao]

        });
    }

    /**
     * Save product
     */
    savePatient(): void {
        const data = this.patientForm.getRawValue();
        this.patientService.savePatient(data)
            .then(() => {

                // Trigger the subscription with new data
                this.patientService.onProductChanged.next(data);

                // Show the success message
                this._matSnackBar.open('Paciente alterado com sucesso.', 'OK', {
                    verticalPosition: 'top',
                    duration: 2000
                });
                this._router.navigate(['list/patients']);
            });
    }

    /**
     * Add product
     */
    addPatient(): void {
        const data = this.patientForm.getRawValue();

        this.patientService.addPatient(data)
            .then(() => {
                console.log(data);

                // Trigger the subscription with new data
                this.patientService.onProductChanged.next(data);

                // Show the success message
                this._matSnackBar.open('Paciente adicionado', 'OK', {
                    verticalPosition: 'top',
                    duration: 2000
                });
                this._router.navigate(['list/patients']);

            });
    }

    deletePatient(): void {
        const data = this.patientForm.getRawValue();

        this.patientService.deletePatient(data)
            .then(() => {

                // Trigger the subscription with new data
                this.patientService.onProductChanged.next(data);

                // Show the success message
                this._matSnackBar.open('Produto apagado com sucesso.', 'OK', {
                    verticalPosition: 'top',
                    duration: 2000
                });
                this._router.navigate(['list/patients']);
            });

    }

    handleGenero(genero: any): void {
        this.patientForm.patchValue({genero: genero});
    }

    handleFuma(fuma: any): void {
        this.patientForm.patchValue({fuma: fuma});
    }

    handleGravida(gravida: any): void {
        this.patientForm.patchValue({gravida: gravida});
    }

    handleEstomago(estomago: any): void {
        this.patientForm.patchValue({estomago: estomago});
    }

    handleHalito(halito: any): void {
        this.patientForm.patchValue({halito: halito});
    }

    handleSensibilidade(sensibilidade: any): void {
        this.patientForm.patchValue({sensibilidade: sensibilidade});
    }

    handleGengivas(gengivas: any): void {
        this.patientForm.patchValue({gengivas: gengivas});
    }

    handleFio(fio: any): void {
        this.patientForm.patchValue({fio: fio});
    }

    handleCoracao(coracao: any): void {
        this.patientForm.patchValue({coracao: coracao});
    }

    handleInfecta(infecta: any): void {
        this.patientForm.patchValue({infecta: infecta});
    }

    handleAlteracoes(alteracoes: any): void {
        this.patientForm.patchValue({alteracoes: alteracoes});
    }

    onFumaChange(): void {
        console.log(this.fuma);

    }

    onChange(event: MatCheckboxChange): void {
        const alergiasForm = this.patientForm.get('alergias') as FormArray;

        if (event.checked) {
            alergiasForm.push(new FormControl(event.source.value));
        } else {
            const i = alergiasForm.controls.findIndex(x => x.value === event.source.value);
            alergiasForm.removeAt(i);
        }
    }

    onChangeAlteracao(event: MatCheckboxChange): void {
        const alteracoesForm = this.patientForm.get('alteracoes') as FormArray;

        if (event.checked) {
            alteracoesForm.push(new FormControl(event.source.value));
        } else {
            const i = alteracoesForm.controls.findIndex(x => x.value === event.source.value);
            alteracoesForm.removeAt(i);
        }
    }

    validaAlteracao(alteracao: any): boolean {
        const alteracoesForm = this.patientForm.get('alteracoes') as FormArray;
        const result = alteracoesForm.getRawValue().find(elem => elem === alteracao);
        return !(result === undefined);

    }

    validaAlergia(alergia: any): boolean {
        const alergiasForm = this.patientForm.get('alergias') as FormArray;
        const result = alergiasForm.getRawValue().find(elem => elem === alergia);
        return !(result === undefined);
    }

    handleRecomendacao(recomendacao: any): void {
        this.patientForm.patchValue({recomendacao: recomendacao});

    }

    handleSeguradora(seguradora: any): void {
        this.seguradora = seguradora;
        this.patientForm.patchValue({seguradora: seguradora});
        if(seguradora === 'Não tem') {
            this.patientForm.patchValue({seguro: seguradora});

        }

    }
}
