import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {DatePipe} from '@angular/common';

@Injectable()
export class PatientService implements Resolve<any> {
    routeParams: any;
    patient: any;
    onProductChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private datePipe: DatePipe
    ) {
        // Set the defaults
        this.onProductChanged = new BehaviorSubject({});
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        this.routeParams = route.params;

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getPatient()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get product
     *
     * @returns {Promise<any>}
     */
    getPatient(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.routeParams.id === 'new') {
                this.onProductChanged.next(false);
                resolve(false);
            } else {
                this._httpClient.get(environment.apiUrl + 'patients/' + this.routeParams.id)
                    .subscribe((response: any) => {
                        this.patient = response.data[0];
                        this.onProductChanged.next(this.patient);
                        resolve(response);
                    }, reject);
            }
        });
    }

    /**
     * Save patient
     *
     * @param patient
     * @returns {Promise<any>}
     */
    savePatient(patient): Promise<any> {
        console.log(patient);
        return new Promise((resolve, reject) => {
            const headers = {
                'Authorization': 'Bearer my-token',
                'Paciente': patient.id.toString(),
                'Contacto': patient.contacto,
                'Nome': patient.nome,
                'Genero': patient.genero,
                'Cidade': patient.cidade,
                'Email': patient.email,
                'Morada': patient.morada,
                'Postal': patient.postal.toString(),
                'Pais': patient.pais,
                'Nascimento': patient.nascimento._i ? patient.nascimento._i.toLocaleString() : this.datePipe.transform(patient.nascimento, 'yyyy-MM-dd'),
                'Seguro': patient.seguro,
                'Nif': patient.nif.toString(),
                fuma: patient.fuma,
                cigarros: patient.cigarros,
                gravida: patient.gravida,
                estomago: patient.estomago,
                detalhe: patient.detalhe,
                halito: patient.halito,
                sensibilidade: patient.sensibilidade,
                gengivas: patient.gengivas,
                dentes: patient.dentes,
                fio: patient.fio,
                ultima: patient.ultima,
                queixas: patient.queixas,
                tratamentos: patient.tratamentos,
                alergias: JSON.stringify(patient.alergias),
                medicacaoescolha: patient.medicacaoescolha,
                medicacao: patient.medicacao,
                coracaoescolha: patient.coracaoescolha,
                coracao: patient.coracao,
                infectaescolha: patient.infectaescolha,
                infecta: patient.infecta,
                alteracoes: JSON.stringify(patient.alteracoes),
                motivacao: patient.motivacao,
                sorriso: patient.sorriso,
                recomendacao: patient.recomendacao,
                seguradora: patient.seguradora,
                profissao: patient.profissao
            };
            const body = {title: 'Angular POST Request Example'};
            console.log('headers', headers);
            this._httpClient.post(environment.apiUrl + 'patients/update', body, {headers})
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Add product
     *
     * @param product
     * @returns {Promise<any>}
     */
    addPatient(patient): Promise<any> {
        return new Promise((resolve, reject) => {
            const headers = {
                'Authorization': 'Bearer my-token',
                'Contacto': patient.contacto,
                'Nome': patient.nome,
                'Genero': patient.genero,
                'Cidade': patient.cidade,
                'Email': patient.email,
                'Morada': patient.morada,
                'Postal': patient.postal.toString(),
                'Pais': patient.pais,
                'Nascimento': patient.nascimento._i.toLocaleString(),
                'Seguro': patient.seguro,
                'Nif': patient.nif.toString(),
                fuma: patient.fuma,
                cigarros: patient.cigarros,
                gravida: patient.gravida,
                estomago: patient.estomago,
                detalhe: patient.detalhe,
                halito: patient.halito,
                sensibilidade: patient.sensibilidade,
                gengivas: patient.gengivas,
                dentes: patient.dentes,
                fio: patient.fio,
                ultima: patient.ultima,
                queixas: patient.queixas,
                tratamentos: patient.tratamentos,
                alergias: JSON.stringify(patient.alergias),
                medicacaoescolha: patient.medicacaoescolha,
                medicacao: patient.medicacao,
                coracaoescolha: patient.coracaoescolha,
                coracao: patient.coracao,
                infectaescolha: patient.infectaescolha,
                infecta: patient.infecta,
                alteracoes: JSON.stringify(patient.alteracoes),
                motivacao: patient.motivacao,
                sorriso: patient.sorriso,
                recomendacao: patient.recomendacao,
                seguradora: patient.seguradora,
                profissao: patient.profissao,
            };
            const body = {title: 'Angular POST Request Example'};
            console.log('headers', headers);
            this._httpClient.post(environment.apiUrl + 'patients', body, {headers})
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    deletePatient(patient): Promise<any> {
        return new Promise((resolve, reject) => {
            const headers = {
                'Authorization': 'Bearer my-token',
                'Id': patient.id.toString()
            };
            const body = {title: 'Angular POST Request Example'};
            console.log('headers', headers);
            this._httpClient.post(environment.apiUrl + 'patients/delete', body, {headers})
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }
}
