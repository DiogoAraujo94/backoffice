import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import {Consult} from '../add-consult/consult.model';

@Component({
    selector     : 'consult',
    templateUrl  : './consult.component.html',
    styleUrls    : ['./consult.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ConsultComponent implements OnInit
{
    @Input()
    cardId;
    @Input()
    consultas: Consult[];
    @Output()
    onConsultDelete = new EventEmitter<any>();

    card: any;
    board: any;

    /**
     * Constructor
     *
     * @param {ActivatedRoute} _activatedRoute
     */
    constructor(
        private _activatedRoute: ActivatedRoute
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // this.board = this._activatedRoute.snapshot.data.board;
        this.card = this.cardId;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Is the card overdue?
     *
     * @param cardDate
     * @returns {boolean}
     */
    isOverdue(cardDate): boolean
    {
        return moment() > moment(new Date(cardDate));
    }

    deleteCard(cardId: any): void {
         this.consultas.map(consult => {
            console.log('consulta: ', consult);
            const index = consult.idCards.map((item) => item).indexOf(cardId);
            if (index > -1) {
                consult.idCards.splice(index, 1);
            }
        });
        this.onConsultDelete.next();
    }
}
