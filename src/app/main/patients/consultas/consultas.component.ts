import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

import {FuseConfirmDialogComponent} from '@fuse/components/confirm-dialog/confirm-dialog.component';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';

import {ScrumboardService} from 'app/main/apps/scrumboard/scrumboard.service';
import {ConsultasService} from './consultas.service';
import {Treatment} from './add-consult/treatment.model';
import {ProposalComponent} from '../proposal/proposal.component';

@Component({
    selector: 'consultas',
    templateUrl: './consultas.component.html',
    styleUrls: ['./consultas.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class ConsultasComponent implements OnInit, OnDestroy {
    board: any;
    dialogRef: any;

    @Input()
    consulta;
    private _prods: any[];
    @Input()
    consultas;
    consultas2: any[];
    @Output()
    onConsultChange = new EventEmitter<any>();


    get prods(): any[] {
        return this._prods;
    }

    @Input()
    set prods(value) {
        this._prods = value;
    }

    @ViewChild(FusePerfectScrollbarDirective)
    listScroll: FusePerfectScrollbarDirective;

    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;

    // Private
    private _unsubscribeAll: Subject<any>;
    private treatments: Treatment[] = [];

    /**
     * Constructor
     *
     * @param {ActivatedRoute} _activatedRoute
     * @param {ScrumboardService} consultasService
     * @param {MatDialog} _matDialog
     */
    constructor(
        private _activatedRoute: ActivatedRoute,
        private consultasService: ConsultasService,
        private _matDialog: MatDialog,
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        console.log('prods consultas: ', this._prods);
        this.consultasService.onBoardChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(board => {
                this.board = board;
            });
        this.consultas2 = this.consultas;
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * On list name changed
     *
     * @param newListName
     */
    onListNameChanged(newListName): void {
        this.consulta.name = newListName;
    }

    /**
     * On card added
     *
     * @param newCardName
     */
    onCardAdd(newCardName): void {
        if (newCardName === '') {
            return;
        }

        const treatment = new Treatment({name: newCardName});
        this.consulta.idCards.push(treatment);
        console.log(this.consulta);
        this.onConsultChange.next(this.consulta.idCards);
        // this.consultasService.addCard(this.consulta.id, treatment);

        setTimeout(() => {
            this.listScroll.scrollToBottom(0, 400);
        });

    }

    /**
     * Remove list
     *
     * @param listId
     */
    removeList(listId, consultas): void {
        this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
            disableClose: false
        });

        this.confirmDialogRef.componentInstance.confirmMessage = 'Tem a certeza que pretende apagar esta consulta? Por favor certifique-se que não existem tratamentos na consulta.';

        this.confirmDialogRef.afterClosed().subscribe(result => {
            if (result) {
                const index = consultas.map((item) => item.id).indexOf(listId);
                if (index > -1) {
                    consultas.splice(index, 1);
                }
            }
        });
    }

    /**
     * On drop
     *
     * @param ev
     */
    onDrop(ev): void {
        // this._scrumboardService.updateBoard();
    }

    onConsultDelete(): void {
        this.onConsultChange.next(this.consulta.idCards);


    }
}
