import {Component, EventEmitter, Input, OnInit, Output, ViewChild, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Product} from '../../product/product.model';

@Component({
    selector: 'add-consult',
    templateUrl: './add-consult.component.html',
    styleUrls: ['./add-consult.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AddConsultComponent implements OnInit {
    formActive: boolean;
    form: FormGroup;

    @Output()
    consultAdded: EventEmitter<any>;
    private _prods: any[];
    prodsSelected: any[] = [];
    @Input()
    consultas: any[];

    @ViewChild('nameInput')
    nameInputField;

    get prods(): any[] {
       return this._prods;
    }

    @Input()
    set prods(value: any[]) {
        this._prods = value;
    }

    ngOnInit(): void {
        console.log('consultas: ', this.consultas);
        this.consultas.forEach(cons => cons.idCards.forEach(card => {
            const index = this.prodsSelected.indexOf(card.name);
            if (index !== -1) {
                this.prodsSelected.splice(index, 1);

            }
        }));
        console.log('prods: ', this.prodsSelected);
    }

    /**
     * Constructor
     *
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _formBuilder: FormBuilder
    ) {
        // Set the defaults
        this.formActive = false;
        this.consultAdded = new EventEmitter();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Open the form
     */
    openForm(): void {
        this.form = this._formBuilder.group({
            name: ''
        });
        this.formActive = true;
        // this.focusNameField();
    }

    /**
     * Close the form
     */
    closeForm(): void {
        this.formActive = false;
    }

    /**
     * Focus to the name field
     */
    focusNameField(): void {
        setTimeout(() => {
            this.nameInputField.nativeElement.focus();
        });
    }

    /**
     * On form submit
     */
    onFormSubmit(): void {
        if (this.form.valid) {
            const cardName = this.form.getRawValue().name;
            this.consultAdded.next(cardName);
            const index = this.prodsSelected.indexOf(cardName);
            this.prodsSelected.splice(index, 1);
            this.formActive = false;
        }
    }

    handleProd(produto: Product): void {
        const prod = produto.nome + ' | ' + produto.dente;
        this.form.patchValue({name: prod});


    }
}

