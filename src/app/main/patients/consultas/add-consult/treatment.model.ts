import {FuseUtils} from '@fuse/utils';

export class Treatment {
    id: string;
    name: string;

    /**
     * Constructor
     *
     * @param consult
     */
    constructor(treatment)
    {
        this.id = treatment.id || FuseUtils.generateGUID();
        this.name = treatment.name || '';
    }
}
