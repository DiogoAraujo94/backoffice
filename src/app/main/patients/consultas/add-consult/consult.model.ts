import {FuseUtils} from '@fuse/utils';

export class Consult {
    id: string;
    name: string;
    idCards: string[];

    /**
     * Constructor
     *
     * @param consult
     */
    constructor(consult)
    {
        this.id = consult.id || FuseUtils.generateGUID();
        this.name = consult.name || '';
        this.idCards = [];
    }
}
