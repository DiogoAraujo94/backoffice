import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {FuseConfigService} from '@fuse/services/config.service';
import {fuseAnimations} from '@fuse/animations';
import {UserService} from './user.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Router} from '@angular/router';

@Component({
    selector: 'login',
    templateUrl: './my-login.component.html',
    styleUrls: ['./my-login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class MyLoginComponent implements OnInit {
    loginForm: FormGroup;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     * @param _userService
     * @param _matSnackBar
     * @param _router
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private _userService: UserService,
        private _matSnackBar: MatSnackBar,
        private _router: Router
) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.loginForm = this._formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });
    }

    login(): void {
        const data = this.loginForm.getRawValue();
        console.log(data);
        this._userService.login(data.email, data.password)
            .then(async (res) => {
                console.log('res:', res);
                if (res.success === 1) {
                    // Show the success message
                    this._matSnackBar.open('Login efectuado com sucesso.', 'OK', {
                        verticalPosition: 'top',
                        duration: 2000
                    });
                    this._router.navigate(['list/proposals']);
                } else {
                    this._matSnackBar.open('Email/Password errados. Por favor tente novamente', 'ERRO', {
                        verticalPosition: 'top',
                        duration: 2000
                    });
                }

            });


    }
}
