import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable()
export class UserService implements Resolve<any> {
    user: any;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient,
        private _router: Router,
    ) {
    }

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    }


    login(email: any, pass: any): Promise<any> {
        console.log('login: ', email);

        return new Promise((resolve, reject) => {
            const headers = {
                'Authorization': 'Bearer my-token',
                email: email,
                pass: pass
            };
            const body = {title: 'Angular POST Request Example'};
            console.log('headers', headers);
            this._httpClient.post(environment.apiUrl + 'users/login', body, {headers})
                .subscribe((response: any) => {
                    this.user = response;
                    localStorage.setItem('user', JSON.stringify(this.user));
                    if (response.success !== 1) {
                        this.user = undefined;
                        localStorage.removeItem('user');

                    }
                    resolve(response);
                }, reject);
        });
    }

    logout(): void {
        // Depois inativar o token
        localStorage.removeItem('user');
        this._router.navigate(['/']);

    }

    getUser(): any {
        if (!this.user) {
            const userData = localStorage.getItem('user');
            this.user = userData ? JSON.parse(userData) : undefined;
        }
        return this.user;
    }
}
