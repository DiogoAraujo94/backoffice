import {Component, ElementRef, Input, Renderer2, ViewEncapsulation} from '@angular/core';
import {FuseNavigationService} from '../../../../@fuse/components/navigation/navigation.service';
import {UserService} from '../../../main/login/user.service';

@Component({
    selector: 'navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class NavbarComponent {
    // Private
    _variant: string;
    private user: any;

    /**
     * Constructor
     *
     * @param {ElementRef} _elementRef
     * @param {Renderer2} _renderer
     */
    constructor(
        private _elementRef: ElementRef,
        private _renderer: Renderer2,
        private _fuseNavigationService: FuseNavigationService,
        private _userService: UserService
    ) {
        this.user = this._userService.getUser();

        // Set the private defaults
        this._variant = 'vertical-style-1';
        if (this.user.credenciais === 'USER') {
            this._fuseNavigationService.removeNavigationItem('clinics');
            this._fuseNavigationService.removeNavigationItem('doctors');
        }
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Variant
     */
    get variant(): string {
        return this._variant;
    }

    @Input()
    set variant(value: string) {
        // Remove the old class name
        this._renderer.removeClass(this._elementRef.nativeElement, this.variant);

        // Store the variant value
        this._variant = value;

        // Add the new class name
        this._renderer.addClass(this._elementRef.nativeElement, value);
    }
}
